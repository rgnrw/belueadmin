import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { AddItemComponent } from './add-item/add-item.component';
import { FormsModule } from '@angular/forms';
import { EditItemComponent } from './edit-item/edit-item.component';

@NgModule({
  imports: [
    CommonModule,
    ItemsRoutingModule,
    FormsModule
  ],
  declarations: [ItemsComponent, AddItemComponent, EditItemComponent]
})
export class ItemsModule { }
