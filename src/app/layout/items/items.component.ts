import { Component, OnInit } from '@angular/core';
import { MatTabsModule } from '@angular/material';
import { classToPlain, plainToClass } from 'class-transformer';
import { MenubyResIdService } from '../../services/MenubyResIdService';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import {ResItemData, ResItems} from '../../model/ResItems';
import {ResItemService} from '../../services/ResItemService.service';

@Component({
selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
  providers: [ResItemService]
})
export class ItemsComponent implements OnInit {

  itemdata: ResItemData[];
  catid: number;
  constructor(public router: Router,public itemservice: ResItemService, public activated: ActivatedRoute) {
   }

  ngOnInit() {
    this.catid = this.activated.snapshot.params.id;
    this.itemservice.getItemsbyIdApi(this.catid)
    .subscribe(
      res=> {
        const itemrespone = plainToClass(ResItems, res as object);
        if(res.error === true)
        {
        }
        else
        {
        this.itemdata = itemrespone.data;
        }
      },
      error=> {},
      ()=> {}
    );

  }

deleteitem(id)
{
this.itemservice.deleteItembyIdApi(id)
.subscribe(
  res=>{
    if(res.error === true)
    {
   
    }
    else{
     this.ngOnInit();
    }
  },
  error=>{},
  ()=> {}
);
}


}
