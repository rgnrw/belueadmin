import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { ResItemService } from '../../../services/ResItemService.service';
import { ResItemData, ResItems } from '../../../model/ResItems';
import { plainToClass } from 'class-transformer';
import { FileUploadService } from '../../../services/FileUpload.service';
import { FileUrl, FileResponse } from '../../../model/FileResponse';
@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss'],
  providers: [ResItemService,FileUploadService],
  exportAs: 'ngForm'
})
export class EditItemComponent implements OnInit {

  imageError = false;
  isImage = false;
  file: File;
  fileurl: FileUrl;
  filename: string;
  isFile = false;
  edititem: ResItemData;
  @ViewChild('fileInput')
  fileInput: ElementRef;
  constructor(public router: Router, public fileservice: FileUploadService,
    public activated: ActivatedRoute, public itemservice: ResItemService) { }

  ngOnInit() {
    this.edititem = new ResItemData();
    this.edititem.categoryId = this.activated.snapshot.params.id;
    this.edititem.id = this.activated.snapshot.params.editid;

    this.itemservice.getItembyItemIdApi(this.edititem.id)
    .subscribe(
      res=> {
        const edititemresponse = plainToClass(ResItemData ,res.data as object);
        console.log(edititemresponse);
        if(res.error === true)
        {
        alert(res.error)
        }
        else{
        this.edititem = edititemresponse;
          console.log(this.edititem.name);
        }
      },
      error=> {},
      ()=> {}
    );
  }

  editCategoryResponse() {
    this.itemservice.updateItembyIdApi(this.edititem)
      .subscribe(
      res => {
        if (res.error === true) {
          console.log("Got error in api");
          alert(res.error);
        }
        else {
          this.router.navigate(['/items',this.edititem.categoryId]);
        }
      },
      error => { },
      () => { }
      );
  }


  previewImage(event: any) {
    /* this.isFile = true; */
    this.imageError = false;
    this.fileInput.nativeElement.src = URL.createObjectURL(event.target.files[0]);
    this.file = event.target.files[0];
    this.filename = this.file.name;
    console.log(this.file);
    console.log(this.filename);
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('filetype', '1');
    this.fileservice.uploadFile(formData).subscribe(
      res => {
        if (res.error === true) {
          console.log("file upload error");
        }
        else {
          console.log("success");
          const fileresponse = plainToClass(FileResponse, res as Object);
          this.fileurl = fileresponse.data;
          console.log(this.fileurl.url);
          this.edititem.icon = this.fileurl.url;
          console.log(this.edititem.icon);
          this.isImage = true;
        }
      },
      error => { console.log("file api hit error"); },
      () => { }
    );
  }






}
