import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ItemsComponent} from './items.component'
import { AddItemComponent} from './add-item/add-item.component';
import { EditItemComponent} from './edit-item/edit-item.component'
const routes: Routes = [
  {path: '', component: ItemsComponent},
  { path: 'additem', component: AddItemComponent},
  {path: 'edititem/:editid', component: EditItemComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }
