import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import {ResItemService} from '../../../services/ResItemService.service';
import { plainToClass,classToPlain } from 'class-transformer';
import {ResItemData, ResItems} from '../../../model/ResItems';
import { FileUploadService } from '../../../services/FileUpload.service';
import { FileUrl, FileResponse } from '../../../model/FileResponse';
@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
  exportAs: 'ngForm',
  providers: [ResItemService,FileUploadService]
})
export class AddItemComponent implements OnInit {
  imageError = false;
  isImage = false;
  file: File;
  fileurl: FileUrl;
  filename: string;
  isFile = false;
  additem: ResItemData;
  @ViewChild('fileInput')
  fileInput: ElementRef;
  constructor(public router: Router, public fileservice:FileUploadService,
    public activated: ActivatedRoute, public itemservice: ResItemService) { }

  ngOnInit() {
    this.additem = new ResItemData();
    this.additem.categoryId= this.activated.snapshot.params.id;
    console.log(this.additem.categoryId);
  }

  postCategoryResponse()
  {
    this.itemservice.addItembyIdApi(this.additem)
    .subscribe(
      res=> {
        if(res.error === true)
        {
          console.log("Got error in api");
          alert(res.error);
        }
        else{
          this.router.navigate(['/items',this.additem.categoryId]);
        }
      },
      error=> {},
      ()=> { }
    );
  }

  previewImage(event: any) {
    /* this.isFile = true; */
    this.imageError = false;
    this.fileInput.nativeElement.src = URL.createObjectURL(event.target.files[0]);
    this.file = event.target.files[0];
    this.filename = this.file.name;
    console.log(this.file);
    console.log(this.filename);
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('filetype', '1');
    this.fileservice.uploadFile(formData).subscribe(
      res => {
        if (res.error === true) {
          console.log("file upload error");
        }
        else {
          console.log("success");
          const fileresponse = plainToClass(FileResponse, res as Object);
          this.fileurl = fileresponse.data;
          console.log(this.fileurl.url);
          this.additem.icon = this.fileurl.url;
          console.log(this.additem.icon);
          this.isImage = true;
        }
      },
      error => { console.log("file api hit error"); },
      () => { }
    );
  }




}
