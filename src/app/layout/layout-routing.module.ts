import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'home', loadChildren: './home/home.module#HomeModule'},
            {path: 'tableinfo', loadChildren: './tableinfo/tableinfo.module#TableinfoModule'},
            {path: 'menus', loadChildren: './menus/menus.module#MenusModule'},
            {path:  'categories', loadChildren: './categories/categories.module#CategoriesModule'},
            {path: 'items/:id', loadChildren: './items/items.module#ItemsModule'},
            { path: 'orders', loadChildren: './orders/orders.module#OrdersModule' },
            { path: 'orderhistory', loadChildren: './orderhistory/orderhistory.module#OrderhistoryModule' },
            { path: 'stats', loadChildren: './stats/stats.module#StatsModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'profile', loadChildren: './profile/profile.module#ProfileModule'},
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {

    constructor() {
        console.log('layoutrouting module is loading in memory');
     }
}