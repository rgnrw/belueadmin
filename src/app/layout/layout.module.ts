import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent, SidebarComponent } from '../shared';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CardService} from '../services/CardService.service';
import { MessagingService } from '../messaging.service';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { ToastOptions } from 'ng2-toastr';
import { CustomOption } from '../model/ToastOptions';
@NgModule({
    imports: [
        CommonModule,
        NgbDropdownModule.forRoot(),
        LayoutRoutingModule,
        TranslateModule,
        ModalModule.forRoot(),
        ToastModule.forRoot()
    ],
    declarations: [
        LayoutComponent,
        HeaderComponent,
        SidebarComponent
        
    ],
    providers: [CardService, MessagingService, { provide: ToastOptions, useClass: CustomOption}]
})
export class LayoutModule {

    constructor() {

        // console.log('layout module is loading in memory 1');
    }
}
