import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';
import { OrdersDetailsComponent } from './orders-details/orders-details.component';
import { DatePipe } from '@angular/common';
import * as moment from 'moment'; 
import { ModalModule } from 'ngx-bootstrap/modal';
import {FormsModule} from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    OrdersRoutingModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  declarations: [OrdersComponent, OrdersDetailsComponent]
})
export class OrdersModule { }
