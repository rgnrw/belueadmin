import { MessagingService } from './../../messaging.service';
import { element } from 'protractor';
import { UpdateOrderStatus } from './../../model/ResOrders';
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule, ActivatedRoute,ParamMap } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import { UTCDateTime } from '../../Utils/DateTime';
import {ResOrderData,ResOrderResponse,CheckInItem} from '../../model/ResOrders';
import {ResOrderService} from '../../services/ResOrderService';
import { CardService } from '../../services/CardService.service';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment'; 
import { Observable } from 'rxjs/Observable';
import { RouteReuseStrategy } from '@angular/router';
import { CustomerRequestResponse, CustomerRequest } from '../../model/CustomerRequest';

@Component({
    selector: "app-orders",
    templateUrl: "./orders.component.html",
    styleUrls: ["./orders.component.scss"],
    providers: [ResOrderService]
})
export class OrdersComponent implements OnInit {
    reqCount = 0;
    current_count = 0;
    newOrderCount = 0;
    inProgressCount = 0;
    requestList: CustomerRequest[];
    statusId: number;
    checkinids: number[];
    orderdata: ResOrderData[];
    cancelledOrderData: ResOrderData[];
    checkInItems: CheckInItem[];
    updateStatus: UpdateOrderStatus;

    constructor(
        public router: Router,
        public activated: ActivatedRoute,
        public orderservice: ResOrderService,
        private cardservice: CardService,
        private msgService: MessagingService
    ) {
        // this.reqCount = this.cardservice.requests;
        // this.newOrderCount = this.cardservice.new_orders;
        // this.inProgressCount = this.cardservice.order_inprogress;
        // this.current_count = this.cardservice.current_orders;
    }

    subscibeParams() {
        this.activated.params.subscribe(params => {
            this.statusId = +this.activated.snapshot.params.id;
            this.onStatusIdReceived();
        });

        this.getCardValues();
    }

    onStatusIdReceived() {
        this.msgService.currentMessage.subscribe(res => {
            if (res) {
                this.ngOnInit();
            }
        });

        this.orderdata = null;
        this.checkinids = null;
        this.checkInItems = null;

        switch (this.statusId) {
            case 0:
                this.orderservice.getAllCheckedInOrdersApi().subscribe(
                    res => {
                        if (res.error === true) {
                            alert(res.message);
                        } else {
                            // console.log("initiated order api success");
                            const orderresponse = plainToClass(
                                ResOrderResponse,
                                res as object
                            );
                            this.orderdata = orderresponse.data;

                            this.getOrderData(this.orderdata);
                            // console.log(this.orderdata);
                        }
                    },
                    error => {},
                    () => {}
                );
                break;

            case 1:
                this.orderservice.getResOrdersApi().subscribe(
                    res => {
                        if (res.error === true) {
                            alert(res.message);
                        } else {
                            // console.log("initiated order api success");
                            const orderresponse = plainToClass(
                                ResOrderResponse,
                                res as object
                            );
                            this.orderdata = orderresponse.data;
                            this.getOrderData(this.orderdata);
                        }
                    },
                    error => {},
                    () => {}
                );
                break;
            case 2:
                this.orderservice.getInProgressResOrdersApi().subscribe(
                    res => {
                        if (res.error === true) {
                            alert(res.message);
                        } else {
                            console.log("in progress order api success");
                            const orderresponse = plainToClass(
                                ResOrderResponse,
                                res as object
                            );
                            this.orderdata = orderresponse.data;
                            this.getOrderData(this.orderdata);
                        }
                    },
                    error => {},
                    () => {}
                );

                break;

            case 3:
                this.orderservice.getCompletedResOrdersApi().subscribe(
                    res => {
                        if (res.error === true) {
                            alert(res.message);
                        } else {
                            console.log("Completed order api success");
                            const orderresponse = plainToClass(
                                ResOrderResponse,
                                res as object
                            );
                            this.orderdata = orderresponse.data;
                             this.getOrderData(this.orderdata);
                        }
                    },
                    error => {},
                    () => {
                    //   this.getCancelledOrders();
                    //   this.cancelledOrderData.forEach(element => {
                    //       this.orderdata.push(element);
                    //   });
                    // this.getOrderData(this.orderdata); 
                }
                );
                console.log('Code reach here')
                
                

                break;

            default:
                console.log("entered switch case");
        }
    }

    ngOnInit() {
        this.subscibeParams();
    }

    getOrderData(orderdata: ResOrderData[]) {
        orderdata.forEach(element => {
            if (element.orderStatus === 1) {
                element.orderStatusText = "Initiated";
            }
            if (element.orderStatus === 2) {
                element.orderStatusText = "In Progress";
            }
            if (element.orderStatus === 3) {
                element.orderStatusText = "Completed";
            }
            if (element.orderStatus === 4) {
                element.orderStatusText = "Cancelled";
            }

            if (element.paymentStatusCode === 1) {
                element.paymentStatusText = "Paid";
            }
            if (element.paymentStatusCode === 2) {
                element.paymentStatusText = "Unpaid";
            }
            if (element.paymentStatusCode === 3) {
                element.paymentStatusText = "Partially Paid";
            }

            element.created = UTCDateTime.getStandardTime(
                element.created,
                "DD-MM-YYYY HH:mm:ss",
                "HH:mm A"
            );
        });
        this.checkinids = this.getCheckInIds(orderdata);
        this.checkInItems = this.getCheckInCollection(
            this.checkinids,
            orderdata
        );
    }

    getHeaderText() {
        if (this.statusId === 0) {
            return "All Orders";
        }
        if (this.statusId === 1) {
            return "Inititated Orders";
        }
        if (this.statusId === 2) {
            return "In Progress Orders";
        }
        if (this.statusId === 3) {
            return "Completed Orders";
        }
    }

    getCheckInIds(orderdata: Array<ResOrderData>) {
        // console.log("Function works");
        const ids = new Array<number>();
        for (const order of orderdata) {
            if (ids.indexOf(order.checkinId) === -1) {
                ids.push(order.checkinId);
            }
        }
        return ids;
    }

    getOrdersBasedOnCheckInId(id: number, orderList: Array<ResOrderData>) {
        const orders = new Array<ResOrderData>();
        for (const order of orderList) {
            if (order.checkinId === id) {
                orders.push(order);
            }
        }
        return orders;
    }

    getCheckInCollection(
        checkinids: Array<number>,
        orderList: Array<ResOrderData>
    ) {
        const array = new Array<CheckInItem>();
        checkinids.forEach(element => {
            const orders = this.getOrdersBasedOnCheckInId(element, orderList);
            const item = new CheckInItem();
            item.checkinid = element;
            item.orderdata = orders;
            array.push(item);
        });
        return array;
    }

    cancelOrder(id, number) {
        this.updateStatus = new UpdateOrderStatus();
        this.updateStatus.orderStatusCode = number;
        this.orderservice.changeOrderStatus(id, this.updateStatus).subscribe(
            res => {
                if (res.error === true) {
                    alert(res.message);
                } else {
                    this.ngOnInit();
                }
            },
            error => {},
            () => {}
        );
    }

    changeOrderStatus(id, orderstatus) {
        this.updateStatus = new UpdateOrderStatus();
        if (orderstatus == 1) {
            this.updateStatus.orderStatusCode = 2;
        }
        if (orderstatus == 2) {
            this.updateStatus.orderStatusCode = 3;
        }
        this.orderservice.changeOrderStatus(id, this.updateStatus).subscribe(
            res => {
                if (res.error === true) {
                    alert(res.message);
                } else {
                    this.ngOnInit();
                }
            },
            error => {},
            () => {}
        );
    }

    getCardValues() {
        this.getAllOrders();
        this.getInitiatedOrders();
        this.getInProgressOrdersAPi();
        this.getAllRequests();
    }

    getAllOrders() {
        this.orderservice.getAllCheckedInOrdersApi().subscribe(
            res => {
                if (res.error === true) {
                } else {
                    // console.log("initiated order api success");
                    const orderresponse = plainToClass(
                        ResOrderResponse,
                        res as object
                    );
                    this.orderdata = orderresponse.data;
                    this.current_count = this.orderdata.length;
                    // this.getOrderData(this.orderdata);
                    // console.log(this.orderdata);
                }
            },
            error => {},
            () => {}
        );
    }

    getInitiatedOrders() {
        this.orderservice.getResOrdersApi().subscribe(
            res => {
                if (res.error === true) {
                } else {
                    // console.log("initiated order api success");
                    const orderresponse = plainToClass(
                        ResOrderResponse,
                        res as object
                    );
                    this.orderdata = orderresponse.data;
                    this.newOrderCount = this.orderdata.length;
                    //this.getOrderData(this.orderdata);
                }
            },
            error => {},
            () => {}
        );
    }

    getInProgressOrdersAPi() {
        this.orderservice.getInProgressResOrdersApi().subscribe(
            res => {
                if (res.error === true) {
                } else {
                    // console.log("in progress order api success");
                    const orderresponse = plainToClass(
                        ResOrderResponse,
                        res as object
                    );
                    this.orderdata = orderresponse.data;
                    this.inProgressCount = this.orderdata.length;
                }
            },
            error => {},
            () => {}
        );
    }

    getAllRequests() {
        this.orderservice.getInitiatedRequest().subscribe(
            res => {
                if (res.error === true) {
                } else {
                    const requestResponse = plainToClass(
                        CustomerRequestResponse,
                        res as Object
                    );
                    this.requestList = requestResponse.data;
                    this.reqCount = this.requestList.length;
                }
            },
            error => {},
            () => {
                this.cardservice.setRequests(this.reqCount);
            }
        );
    }

    getCancelledOrders() {
        this.orderservice.getCancelledResOrdersApi().subscribe(
            res => {
                if (res.error === true) {
                    alert(res.message);
                } else {
                    // console.log("Completed order api success");
                    const orderresponse = plainToClass(
                        ResOrderResponse,
                        res as object
                    );
                    this.cancelledOrderData = orderresponse.data;
                    // console.log('Cancelled orders'+this.cancelledOrderData);
                    // this.getOrderData(this.orderdata);
                }
            },
            error => {},
            () => {}
        );

    }
}
