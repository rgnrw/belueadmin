import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import {Location} from '@angular/common';
import {ResOrderData, ResOrderResponse, CheckInItem, Customer,Table} from '../../../model/ResOrders';
import {ResOrderDetails} from "../../../model/ResOrderDetail";
import {CheckOut } from "../../../model/CheckoutResponse";
import { UnpaidOrderData } from './../../../model/ResOrders';
import { SessionData, SessionResponse} from './../../../model/CustomerSession';
import { PaymentMode } from './../../../model/GlobalData';
import { Router, RouterModule, ActivatedRoute} from '@angular/router';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {ResOrderService} from '../../../services/ResOrderService';
import { BsDropdownModule } from 'ngx-bootstrap';
import {GlobalData, GlobalResponse} from '../../../model/GlobalData';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderStatusCode, PaymentStatusCode, SessionStatusCode } from '../../../global';



@Component({
  selector: 'app-orders-details',
  templateUrl: './orders-details.component.html',
  styleUrls: ['./orders-details.component.scss'],
  providers: [ResOrderService]
})
export class OrdersDetailsComponent implements OnInit {
  
  checkout: CheckOut;
  globalData: GlobalData;
  buttonVisible: boolean;
  paymentMode: PaymentMode;
  totalamount : number;
  customer: Customer;
  table: Table;
  checkinid: number;
  orderdata: ResOrderData[];
  unpaidOrder: ResOrderData[];
  jsondata: any;
  paidAmount=0;
  unpaidAmount= 0;
  completeCount= 0;
  paid= false;
  unpaid = false;
  sessiondata: SessionData;
  checkedIn= true;
  modalRef: BsModalRef;
  @ViewChild('template')
  temp: TemplateRef<any>;
  constructor(public orderservice: ResOrderService, public location: Location,public router: Router, 
    public activated: ActivatedRoute, private modalService: BsModalService) { }

  ngOnInit() {
    this.checkinid = this.activated.snapshot.params.checkinid;
    this.checkout = new CheckOut();
    // console.log(this.checkinid);
    this.sessiondata = new SessionData();
    this.getCustomerSession(); 
  
    }
  
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  
  
  checkOutModal()
  {
  // this.unpaidOrder = new Array<ResOrderData>();
  // this.sessiondata.orders.forEach(element => {
  //   if(element.paymentStatusCode !== PaymentStatusCode.PAID && 
  //     element.orderStatus === OrderStatusCode.Completed )
  //   {
  //   this.unpaidOrder.push(element);
  //   }
  // });
    
  this.paid = (this.totalamount == this.paidAmount)?true:false;
  this.modalService.show(this.temp);
    
  }



  checkOut(paymentInfo: string)
  {
    
    this.checkout = new CheckOut();
    this.checkout.checkInId = this.checkinid;
    this.checkout.paidInfo = paymentInfo;
    const jsondata = classToPlain(this.checkout);
    //console.log(jsondata);
    this.orderservice.checkOut(jsondata).subscribe(
      res=> {
        if(res.error === true)
        {
        alert(res.error);
        }
        else{
        //console.log("Check Out");
        this.modalService.hide(1);
        this.location.back();
        }
      },
      error=> {},
      () => { }
    );
  } 

  getAllCategories()
  {

  }

  getOrderDetail() {

          this.totalamount = 0;
          this.paidAmount = 0;
          this.unpaidAmount = 0;
          this.sessiondata.orders.forEach(element => {
            element.orderItemsArray = plainToClass(ResOrderDetails, JSON.parse(element.data));
            // element.parseDataStringToOrderItemsArray(); 
            this.getAllCategories();
            

            if(element.orderStatus !== OrderStatusCode.Cancelled)
            {
            this.totalamount= +((this.totalamount + element.amount).toFixed(2));                    /* for total amount of all orders */
            }   
           this.paidAmount = this.paidAmount + (element.amount - element.remainingAmount)  //Amount paid by customer
          this.paidAmount = +(this.paidAmount.toFixed(2));
            if (element.orderStatus === OrderStatusCode.Initated) {
              element.orderStatusText = 'Initiated';
            }
            if (element.orderStatus === OrderStatusCode.InProgress) {
              element.orderStatusText = 'In Progress';
            }
            if (element.orderStatus === OrderStatusCode.Completed) {
              element.orderStatusText = 'Completed';
            }
            if (element.orderStatus === OrderStatusCode.Cancelled) {
              element.orderStatusText = 'Cancelled';
            }
            if (element.paymentStatusCode === PaymentStatusCode.PAID) {
              element.paymentStatusText = 'Paid';
            }
            if (element.paymentStatusCode === PaymentStatusCode.UNPAID) {
              element.paymentStatusText = 'Unpaid';
            }
            if (element.paymentStatusCode === PaymentStatusCode.PARTIALLY_PAID) {
              element.paymentStatusText = 'Partially Paid';
            }
            
            // if(element.remainingAmount !== 0)
            // {
            // element.remainingStatus = true;
            // }


            if(element.orderStatus == OrderStatusCode.Completed || element.orderStatus == OrderStatusCode.Cancelled )
            {
              this.completeCount = this.completeCount +1;
            }

         });

         this.unpaidAmount = this.totalamount - this.paidAmount;
         if (this.sessiondata.orders.length == this.completeCount) {
             this.buttonVisible = true;
         }
  }
  

  getCustomerSession()
  {
    this.orderservice.getCustomerSession(this.activated.snapshot.params.checkinid)
    .subscribe(
      res=> {
        if(res.error === true)
        {
         
        }
        else
        {
        const sessionresponse = plainToClass(SessionResponse, res as Object);
        this.sessiondata = sessionresponse.data;
        if (this.sessiondata.sessionStatusCode === SessionStatusCode.CHECKED_OUT) {
          this.checkedIn = false;
          //console.log(this.checkedIn);
        }
        
        }
      },
      error=> {},
      ()=> { this.getOrderDetail()  }
    );
  }

  goBack()
  {
  this.location.back();
  }

}
