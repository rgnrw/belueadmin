import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OrdersComponent} from './orders.component';
import {OrdersDetailsComponent} from './orders-details/orders-details.component';

const routes: Routes = [
  { path: ':id', component: OrdersComponent },
  { path: 'orderdetails/:checkinid', component: OrdersDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
