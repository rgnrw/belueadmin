import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsRoutingModule } from './stats-routing.module';
import { StatsComponent } from './stats.component';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import {FormsModule} from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    StatsRoutingModule,
    MyDateRangePickerModule,
    FormsModule
   
  ],
  declarations: [StatsComponent]
})
export class StatsModule { }
