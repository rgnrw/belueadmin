import { Component, OnInit, TemplateRef,Injectable } from '@angular/core';
import { IMyOptions,IMyDrpOptions,MyDateRangePicker, MyDateRangePickerModule} from 'mydaterangepicker';
import {plainToClass} from 'class-transformer';
import { UTCDateTime} from '../../Utils/DateTime';
import {HomeService} from '../../services/Home.service';
import { StatData, StatResponse } from '../../model/Statistics';
import * as moment from 'moment';
@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
  providers: [HomeService]
})



export class StatsComponent implements OnInit {




  myDateRangePickerOptions: IMyDrpOptions = {
    // other options...
    dateFormat: 'dd-mm-yyyy',
    selectorHeight: '300px',
    selectorWidth: '300px',
    height: '36px',
    showSelectDateText: true
  };
 
  
  collectedAmount=0;
  numOfOrders=0;

  currentYear: number;
  currentMonth: number;
  currentDate: number;

  public model: any;
  public dateModel: any;
  statData: StatData;
  constructor(public homeservice: HomeService ) {
    
    const today = new Date();
    this.currentYear = today.getFullYear();
    this.currentMonth= today.getMonth();
    this.currentDate = today.getDate();

    this.model = {
      beginDate: { year: 2018, month: +(this.currentMonth) + 1, day: 1 },
      endDate: { year: 2018, month: +(this.currentMonth) + 1, day: 30 }
    };
  
   }
 
   

  ngOnInit() {
    
  }

  getStatistics()
  {
    this.dateModel = {
      beginDate: {
        year: this.model.beginDate.year, month: +(this.model.beginDate.month) - 1,
        day: this.model.beginDate.day
      },
      endDate: { year: this.model.endDate.year, month: +(this.model.endDate.month) - 1, day: this.model.endDate.day}
    };
    /* console.log(this.dateModel); */

    const startDate = UTCDateTime.getStringDatefromJSON(this.dateModel.beginDate,'DD-MM-YYYY');
    const endDate = UTCDateTime.getStringDatefromJSON(this.dateModel.endDate, 'DD-MM-YYYY');
   /*  console.log(startDate);
    console.log(endDate); */

  this.homeservice.getStatistics(startDate, endDate)
  .subscribe(
    res=> {
      if(res.error === true)
      {
        console.log('stat api hit error');
      }
      else
      {
      const statresponse = plainToClass(StatResponse, res as Object);
      this.statData = statresponse.data;
     // console.log(this.statData);
      this.collectedAmount = this.statData.amount;
      this.numOfOrders = this.statData.numOfOrders;
        
      }
     },
    error=> {},
    ()=> {}
  );
  }


}
