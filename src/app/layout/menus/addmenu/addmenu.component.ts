import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {FormsModule} from '@angular/forms';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {MenubyResId, ResMenuData} from '../../../model/MenubyResId';
import {MenubyResIdService} from '../../../services/MenubyResIdService'

@Component({
  selector: 'app-addmenu',
  templateUrl: './addmenu.component.html',
  styleUrls: ['./addmenu.component.scss'],
  exportAs: 'ngForm',
  providers: [MenubyResIdService]
})
export class AddmenuComponent implements OnInit {

  addmenu: ResMenuData;
  resid: number;
  message: boolean
  constructor(public router: Router, public menuservice: MenubyResIdService) { }
  ngOnInit() {
    this.addmenu= new ResMenuData();
    this.resid = +sessionStorage.getItem("id");
    this.addmenu.restaurantId= this.resid;
  }

  postMenuResponse()
  {
    this.menuservice.addMenubyIdApi(this.addmenu)
    .subscribe(
      res=> {
        if(res.error === true)
        {
        alert(res.message);
        console.log("Got error in api");
        }
        else{
          this.router.navigate(['/menus'])
        }
      },
      error=> {this.menuservice.throwError(error)},
      ()=> { }
    );
  }

}
