import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRestMenuComponent } from './edit-rest-menu.component';

describe('EditRestMenuComponent', () => {
  let component: EditRestMenuComponent;
  let fixture: ComponentFixture<EditRestMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRestMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRestMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
