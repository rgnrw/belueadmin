import { Component, OnInit } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {MenubyResId, ResMenuData} from '../../../model/MenubyResId';
import {MenubyResIdService} from '../../../services/MenubyResIdService';
import { error } from 'selenium-webdriver';
@Component({
  selector: 'app-edit-rest-menu',
  templateUrl: './edit-rest-menu.component.html',
  styleUrls: ['./edit-rest-menu.component.scss'],
  providers: [MenubyResIdService]
})
export class EditRestMenuComponent implements OnInit {


  updatemenu: ResMenuData;
  constructor(public router: Router, public updateservice: MenubyResIdService, public activated: ActivatedRoute) { }

  ngOnInit() {
    this.updatemenu = new ResMenuData();
    this.updatemenu.restaurantId= +sessionStorage.getItem("id");
    this.updatemenu.id = +this.activated.snapshot.params.id;
    this.updateservice.getMenubyMenuIdApi(this.updatemenu.id).
    subscribe(
      res=> {
        const editmenuresponse = plainToClass(ResMenuData,res.data as Object)
        if(res.error === true)
        {
          alert(res.error)
        }
        else
        {
        this.updatemenu = editmenuresponse;
        }

      },
      error=> {},
      ()=> {}
    );
  }


  updateMenuResponse()
  {
  console.log(this.updatemenu);
  this.updateservice.updateMenubyIdApi(classToPlain(this.updatemenu))
  .subscribe(
    res=> {
      console.log(res);
      if(res.error === true)
      {
        alert(res.error);
      }
      else{
        this.router.navigate(['/menus'])
      }
    },
    error=> {this.updateservice.throwError(error)},
    ()=> {}
  );

  }

}
