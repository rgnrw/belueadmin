import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {MenubyResId, ResMenuData} from '../../model/MenubyResId';
import {MenubyResIdService} from '../../services/MenubyResIdService';



@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss'],
  providers: [MenubyResIdService]
})
export class MenusComponent implements OnInit {

  menudata: ResMenuData[];
  message: boolean;
  constructor(public router: Router, public menuservice: MenubyResIdService) { }

  ngOnInit() {
    this.menuservice.getMenubyIdApi()
    .subscribe(
    res=> {
      const menurespone = plainToClass(MenubyResId, res as object);
      console.log(menurespone);
      if(res.error === true)
      {
       /*  console.log("api menu res error"); */
        alert(res.message)
      }
      else{
       /*  console.log("api menu success"); */
      this.menudata = menurespone.data;
      }


     },
    error => { console.log("api hit error"); this.menuservice.throwError(error)},
    ()=> {}
    );

  }

  deleteMenu(id)
  {
    this.menuservice.deleteMenubyIdApi(id)
    .subscribe(
      res=> {
        if(res.error === true)
        {
        alert(res.error)
        }
        else{
          this.ngOnInit()
        }
      },
      error=> {},
      ()=> {}
    );
  }





}
