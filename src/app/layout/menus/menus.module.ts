import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MenusRoutingModule } from './menus-routing.module';
import { MenusComponent } from './menus.component';
import { AddmenuComponent } from './addmenu/addmenu.component';
import { EditRestMenuComponent } from './edit-rest-menu/edit-rest-menu.component';

@NgModule({
  imports: [
    CommonModule,
    MenusRoutingModule,
    FormsModule
  ],
  declarations: [MenusComponent, AddmenuComponent, EditRestMenuComponent]
})
export class MenusModule { }
