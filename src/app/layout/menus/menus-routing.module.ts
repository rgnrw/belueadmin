import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenusComponent } from './menus.component';
import {AddmenuComponent} from './addmenu/addmenu.component';
import {EditRestMenuComponent} from "./edit-rest-menu/edit-rest-menu.component";

const routes: Routes = [
  {path: '', component: MenusComponent},
  {path: 'addmenu', component: AddmenuComponent},
  {path: 'editmenu/:id', component: EditRestMenuComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenusRoutingModule { }
