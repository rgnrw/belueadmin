import { FormComponent } from './../form/form.component';
import { FormsModule, FormGroup } from '@angular/forms';
import { ResTablesData } from './../../model/TablesbyResId';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { plainToClass, classToPlain } from 'class-transformer';
import { Router } from '@angular/router';
import { SettingsService } from './../../services/Settings.service';
import { ChangePassword } from './../../model/Settings';
import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';



@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [SettingsService]
})
export class SettingsComponent implements OnInit {

  incorrectPass= false;
  isSame = false;
  success= false;
  changePassword: ChangePassword;
  jsondata: any;
  newPass: string;

  @ViewChild('settings')
  settings: FormGroup;
  @ViewChild('editpass')
  editpass: ElementRef;
  @ViewChild('changebutton')
  changebutton: ElementRef;
  @ViewChild('change')
  change: ElementRef;
  @ViewChild('cancel')
  cancel: ElementRef;

  constructor(public router:Router, public setting:SettingsService) { }

  ngOnInit() {
    this.changePassword = new ChangePassword();
    this.change.nativeElement.hidden = false;
  }

  changePasswordForm()
  {
  this.incorrectPass = false;
  this.isSame = false;
  this.incorrectPass = false;
  this.editpass.nativeElement.disabled = !this.editpass.nativeElement.disabled;
  this.changebutton.nativeElement.hidden = !this.changebutton.nativeElement.hidden;
  this.change.nativeElement.hidden = !this.change.nativeElement.hidden;
  this.cancel.nativeElement.hidden = !this.cancel.nativeElement.hidden;
  }



  postPasswordResponse()
    {
    if(this.newPass === this.changePassword.newPassword)
    {
    this.changePassword.id = +sessionStorage.getItem('id');
    this.jsondata = classToPlain(this.changePassword);
    this.setting.changePasswordApi(this.jsondata).subscribe(
      res=> {
        if(res.error === true)
        {
        this.incorrectPass = true;
        this.isSame = false;
        }
        else
        {
        this.incorrectPass = false;
        this.isSame = false;
        this.settings.reset();
        this.changePasswordForm();
        }
      },
      error=> {},
      () => {}
    );

    }

    else{
      this.isSame = true;
      this.incorrectPass = false;
    this.success = false;
    }
  }
}
