import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router'
import { FormsModule } from '@angular/forms';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import { ResCategory, ResCategoryData, ResEditCategory } from '../../../model/ResCategory';
import { FileUrl, FileResponse} from '../../../model/FileResponse';
import {ResCategoryService} from '../../../services/ResCategoryService.service';
import { FileUploadService } from '../../../services/FileUpload.service';



@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss'],
  exportAs: 'ngForm',
  providers: [ResCategoryService,FileUploadService]
})

export class AddCategoryComponent implements OnInit {
  
  imageError = false;
  isImage= false;
  file: File;
  fileurl: FileUrl;
  filename: string;
  isFile= false;
  addcategory: ResCategoryData;
  @ViewChild('fileInput') 
  fileInput: ElementRef;
  constructor(public router: Router, public fileservice: FileUploadService ,
    public category: ResCategoryService, public activated: ActivatedRoute) { }

  ngOnInit() {
    this.addcategory= new ResCategoryData();
    this.addcategory.menuId= this.activated.snapshot.params.menuId;
    if(this.activated.snapshot.params.catId)
    {
      this.category
          .getCategoryDetailbyIdApi(this.activated.snapshot.params.catId)
          .subscribe(
              res => {
                  const categoryresponse = plainToClass(
                      ResEditCategory,
                      res as Object
                  );
                  if (res.error === true) {
                      alert(res.error);
                  } else {
                      this.addcategory = categoryresponse.data;
                  }
              },
              error => {},
              () => {}
          );
    }
  }
  
  postCategoryResponse()
  {
    if(this.activated.snapshot.params.catId)
    {

  this.category
      .updateCategorybyIdApi(this.addcategory)
      .subscribe(
          res => {
              if (res.error === true) {
                  alert(res.error);
              } else {
                  this.router.navigate(["/categories"]);
              }
          },
          error => {},
          () => {}
      );
    }
    else {
    this.category.addCategorybyIdApi(this.addcategory)
    .subscribe(
      res=> {
        if(res.error === true)
        {

        }
        else{
          this.router.navigate(['categories'])
        }

      },
      error=> {},
      ()=> {}
    );
  }
 

  }



  previewImage(event: any)
  {
    /* this.isFile = true; */
    this.imageError = false;
    this.fileInput.nativeElement.src = URL.createObjectURL(event.target.files[0]);
    this.file= event.target.files[0];
    this.filename = this.file.name;
    console.log(this.file);
    console.log(this.filename);
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('filetype', '1');
    this.fileservice.uploadFile(formData).subscribe(
      res => {
        if (res.error === true) {
          console.log("file upload error");
        }
        else {
          console.log("success");
          const fileresponse = plainToClass(FileResponse, res as Object);
          this.fileurl = fileresponse.data;
          console.log(this.fileurl.url);
          this.addcategory.icon = this.fileurl.url;
          this.isImage = true;
        }
      },
      error => { console.log("file api hit error"); },
      () => { }
    );
  }


  uploadImage()
  {

    
    
  }



  getHeaderText()
  {
    if(this.activated.snapshot.params.catId)
    {
    return 'Edit Category';
    }
    else{
    return 'Add Category'
    }
  }


}
