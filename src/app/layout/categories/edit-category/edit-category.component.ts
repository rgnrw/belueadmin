import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {ResCategory,ResCategoryData} from '../../../model/ResCategory';
import { FileUrl, FileResponse } from '../../../model/FileResponse';
import {ResCategoryService} from '../../../services/ResCategoryService.service';
import { FileUploadService } from '../../../services/FileUpload.service';
@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss'],
  providers: [ResCategoryService, FileUploadService]
})
export class EditCategoryComponent implements OnInit {

  imageError = false;
  isImage = false;
  file: File;
  fileurl: FileUrl;
  filename: string;
  isFile = false;
  updatecategory: ResCategoryData;
  @ViewChild('fileInput')
  fileInput: ElementRef;
  constructor(public router: Router, public fileservice:FileUploadService, 
    public updatecategoryservice: ResCategoryService, public activated: ActivatedRoute) { }

  ngOnInit() {
  this.updatecategory= new ResCategoryData();
  this.updatecategory.id=  this.activated.snapshot.params.id;
  this.updatecategory.menuId= this.activated.snapshot.params.menuid;
 
    this.updatecategoryservice.getCategoryDetailbyIdApi(this.updatecategory.id)
    .subscribe(
      res=> {
        const categoryresponse = plainToClass(ResCategoryData ,res.data as Object);
        if(res.error === true)
        {
          alert(res.error);
        }
        else{
          this.updatecategory = categoryresponse;
        }
      },
      error=> {},
      ()=> { }
    );
  }


  updateCategoryResponse()
  {

  this.updatecategoryservice.updateCategorybyIdApi(this.updatecategory)
  .subscribe(
    res=> {
      if(res.error === true)
      {
        alert(res.error)
      }
      else{
        this.router.navigate(['/categories'])
      }


    },
    error=> {},
    () => {}
  );

  }




  previewImage(event: any) {
    /* this.isFile = true; */
    /* this.imageError = false; */
    this.updatecategory.icon = null;
    this.fileInput.nativeElement.src = URL.createObjectURL(event.target.files[0]);
    this.file = event.target.files[0];
    this.filename = this.file.name;
    console.log(this.file);
    console.log(this.filename);

    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('filetype', '1');
    this.fileservice.uploadFile(formData).subscribe(
      res => {
        if (res.error === true) {
          console.log("file upload error");
        }
        else {
          console.log("success")
          const fileresponse = plainToClass(FileResponse, res as Object);
          this.fileurl = fileresponse.data;
          console.log(this.fileurl.url);
          this.updatecategory.icon = this.fileurl.url;
          this.imageError = false;
          this.isImage = true;
        }
      },
      error => { console.log("file api hit error"); },
      () => { }
    );
   
  }


  /* uploadImage() {

    
  } */

}
