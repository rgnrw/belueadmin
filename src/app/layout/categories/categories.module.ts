import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import {MatTabsModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AddCategoryComponent } from './add-category/add-category.component';
import {FormsModule} from '@angular/forms';
import { EditCategoryComponent } from './edit-category/edit-category.component';
@NgModule({
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    MatTabsModule,
    FormsModule
  ],
  declarations: [CategoriesComponent, AddCategoryComponent, EditCategoryComponent]
})
export class CategoriesModule { }
