import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent} from './categories.component';
import { AddCategoryComponent} from './add-category/add-category.component';
import {EditCategoryComponent} from './edit-category/edit-category.component';

const routes: Routes = [
  {path:'', component: CategoriesComponent},
  {path:'addcategory/:menuId', component: AddCategoryComponent},
  {path:'editcategory/:catId/:menuId' , component: AddCategoryComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
