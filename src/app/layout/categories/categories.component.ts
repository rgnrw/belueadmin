import { Component, OnInit } from '@angular/core';
import { MatTabsModule } from '@angular/material';
import {classToPlain, plainToClass} from 'class-transformer';
import {MenubyResIdService} from '../../services/MenubyResIdService';
import { Router, RouterModule } from '@angular/router';
import { MenubyResId, ResMenuData } from '../../model/MenubyResId';
import {ResCategory,ResCategoryData } from '../../model/ResCategory';
import {ResCategoryService} from '../../services/ResCategoryService.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [MenubyResIdService, ResCategoryService]
})
export class CategoriesComponent implements OnInit {

  static obj = 12;
  index: number;
  menuid: number;
  menudata: ResMenuData[];
  categorydata: ResCategoryData[];
  constructor(public router: Router, public menuservice: MenubyResIdService, public ctgservice: ResCategoryService) { }

  ngOnInit() {
    this.menuservice.getMenubyIdApi()
      .subscribe(
      res => {
        const menurespone = plainToClass(MenubyResId, res as object);
        if (res.error === true) {
          /*  console.log("api menu res error"); */
          alert(res.message)
        }
        else {
          /*  console.log("api menu success"); */
          this.menudata = menurespone.data;
          // for // when i == 0, menu.isSelected = true;
          /* console.log(this.menudata.length); */
          // console.log(ResMenuData.length);
          for(let i=0;i<this.menudata.length;i++)
          {
          this.menudata[0].isSelected= true;
          this.menudata[i].isSelected= false;
          }
          let id = this.menudata[0].id;
          this.changecategory(0, id)
        }

      },
      error => { console.log("api hit error"); this.menuservice.throwError(error) },
      () => { }
      );
  }

  changecategory(i,menuid)
  {
    this.index =i ;
    this.menuid= menuid;
    this.categorydata = null;
    for (let j = 0; j < this.menudata.length; j++)
    {
    this.menudata[j].isSelected = false;
    }
    this.menudata[i].isSelected = true;

   this.ctgservice.getCategorybyIdApi(menuid)
   .subscribe(
    res=> {
      const ctgrespone = plainToClass(ResCategory, res as object)
      if(res.error === true)
      {
       this.categorydata =null;
      }
      else
      {
      this.categorydata = ctgrespone.data;
      }

    },
    error=> {},
    ()=> {}

   );
  }

  deleteCategory(id)
  {
  this.ctgservice.deleteCategorybyidApi(id)
  .subscribe(
    res=> {
      if(res.error === true)
      {
        alert(res.error)
      }
      else{
        this.changecategory(this.index, this.menuid);
      }
    },
    error=> {},
    ()=> {}
  );

  }



}
