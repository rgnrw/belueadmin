import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistoryRoutingModule } from './orderhistory-routing.module';
import { OrderhistoryComponent } from './orderhistory.component';
import { HistoryDetailComponent } from './history-detail/history-detail.component';

@NgModule({
  imports: [
    CommonModule,
    OrderhistoryRoutingModule
  ],
  declarations: [OrderhistoryComponent, HistoryDetailComponent]
})
export class OrderhistoryModule { }
