import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { ResOrderData, ResOrderResponse, CheckInItem, Customer, Table } from '../../../model/ResOrders';
import { ResOrderDetails } from "../../../model/ResOrderDetail";
import { CheckOut } from "../../../model/CheckoutResponse";
import { UnpaidOrderData } from './../../../model/ResOrders';
import { PaymentMode } from './../../../model/GlobalData';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import { ResOrderService } from '../../../services/ResOrderService';
@Component({
  selector: 'app-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss'],
  providers: [ResOrderService]
})
export class HistoryDetailComponent implements OnInit {

  checkout: CheckOut;
  totalamount = 0;
  customer: Customer;
  table: Table;
  checkinid: number;
  orderdata: ResOrderData[];
  unpaidOrder: UnpaidOrderData[];
  jsondata: any;

 
  constructor(public orderservice: ResOrderService, public location: Location, public router: Router,
    public activated: ActivatedRoute) { }

  ngOnInit() {
    this.checkinid = this.activated.snapshot.params.checkinid;
    //console.log(this.checkinid);

    this.getOrderDetail();
  }


  getOrderDetail() {
    this.orderservice.getResOrderByCheckinIdApi(this.checkinid)
      .subscribe(
      res => {
        if (res.error === true) {
          alert(res.message);
        }
        else {
          const orderresponse: ResOrderResponse = plainToClass(ResOrderResponse, res as object);
          this.orderdata = orderresponse.data;
          this.orderdata.forEach(element => {
            element.parseDataStringToOrderItemsArray();               /* json string parsing */
            this.totalamount = this.totalamount + element.amount;  /* for total amount of all orders */
            if (element.orderStatus === 3) {
              element.orderStatusText = 'Completed';
            }
            if (element.orderStatus === 1) {
              element.orderStatusText = 'Initiated';
            }
            if (element.orderStatus === 2) {
              element.orderStatusText = 'In Progress';
            }
            if (element.paymentStatusCode === 1) {
              element.paymentStatusText = 'Paid';
            }
            if (element.paymentStatusCode === 2) {
              element.paymentStatusText = 'Unpaid';
            }
          });
          this.customer = this.orderdata[0].customer;
          this.table = this.orderdata[0].table;
        }
      },
      error => { },
      () => {  }
      );

  }


  goBack()
  {
  this.location.back();
  }


}
