import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { plainToClass, classToPlain} from 'class-transformer';
import { ResOrderData, ResOrderResponse, CheckInItem } from '../../model/ResOrders';
import { ResOrderService } from '../../services/ResOrderService';
import { UTCDateTime } from '../../Utils/DateTime';
@Component({
  selector: 'app-orderhistory',
  templateUrl: './orderhistory.component.html',
  styleUrls: ['./orderhistory.component.scss'],
  providers: [ResOrderService]
})
export class OrderhistoryComponent implements OnInit {
  checkinids: number[];
  orderdata: ResOrderData[];
  checkInItems: CheckInItem[];
  constructor(public router: Router, public orderservice: ResOrderService) { }

  ngOnInit() {
    this.orderservice.getOrderHistoryApi()
      .subscribe(
      res => {
        if (res.error === true) {
          alert(res.message);
        }
        else {
          const orderresponse = plainToClass(ResOrderResponse, res as object);
          this.orderdata = orderresponse.data;
          this.orderdata.forEach(element => {
            if(element.orderStatus === 3)
            {
              element.orderStatusText= 'Completed';
            }
            if(element.orderStatus === 4)
            {
              element.orderStatusText= 'Cancelled';
            }
            if (element.orderStatus === 1) {
              element.orderStatusText = 'Initiated';
            }
            if (element.orderStatus === 2) {
              element.orderStatusText = 'In Progress';
            }
            if (element.paymentStatusCode === 1) {
              element.paymentStatusText = 'Paid';
            }
            if (element.paymentStatusCode === 2) {
              element.paymentStatusText = 'Unpaid';
            }
            element.created = UTCDateTime.getStandardTime(element.created, "DD-MM-YYYY HH:mm:ss", "MMM-DD-YYYY HH:mm A" );
          });
          this.checkinids = this.getCheckInIds(this.orderdata);
          /*  console.log(this.checkinids); */
          this.checkInItems = this.getCheckInCollection(this.checkinids, this.orderdata);
          /*           console.log(this.checkInItems); */
        }
      },
      error => { },
      () => { }
      );
  }

  getCheckInIds(orderdata: Array<ResOrderData>) {
    // console.log("Function works");
    const ids = new Array<number>();
    for (const order of orderdata) {
      if (ids.indexOf(order.checkinId) === -1) {
        ids.push(order.checkinId);
      }
    }
    return ids;
  }


  getOrdersBasedOnCheckInId(id: number, orderList: Array<ResOrderData>) {
    const orders = new Array<ResOrderData>();
    for (const order of orderList) {
      if (order.checkinId === id) {
        orders.push(order);
      }
    }
    return orders;

  }

  getCheckInCollection(checkinids: Array<number>, orderList: Array<ResOrderData>) {
    const array = new Array<CheckInItem>();
    checkinids.forEach(element => {
      const orders = this.getOrdersBasedOnCheckInId(element, orderList);
      const item = new CheckInItem();
      item.checkinid = element;
      item.orderdata = orders;
      array.push(item);
    });
    return array;
  }




}
