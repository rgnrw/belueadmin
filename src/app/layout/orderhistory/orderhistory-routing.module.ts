import { HistoryDetailComponent } from './history-detail/history-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OrderhistoryComponent} from './orderhistory.component';

const routes: Routes = [
  { path: '', component: OrderhistoryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistoryRoutingModule { }
