import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRestTableComponent } from './edit-rest-table.component';

describe('EditRestTableComponent', () => {
  let component: EditRestTableComponent;
  let fixture: ComponentFixture<EditRestTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRestTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRestTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
