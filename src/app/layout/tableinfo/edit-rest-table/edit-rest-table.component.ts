import { Component, OnInit } from '@angular/core';
import { Router, RouterModule ,ActivatedRoute, } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import { UpdateResTable, UpdateTableData} from '../../../model/EditResTable';
import {TablebyResIdService} from '../../../services/TablebyResIdService.service';
import { error } from 'util';

@Component({
  selector: 'app-edit-rest-table',
  templateUrl: './edit-rest-table.component.html',
  styleUrls: ['./edit-rest-table.component.scss'],
  providers: [TablebyResIdService],
  exportAs: 'ngForm'
})

export class EditRestTableComponent implements OnInit {

  edittable: UpdateTableData;
  constructor(public tablebyresidservice: TablebyResIdService, public router: Router, public activated: ActivatedRoute) { }

  ngOnInit() {
    this.edittable= new UpdateTableData();
    this.edittable.restaurantId = +sessionStorage.getItem("id");
    this.edittable.id = +this.activated.snapshot.params.id;
    this.tablebyresidservice.getTablebyIdApi(this.edittable.id)
    .subscribe(
      res=> {
        const editresponse = plainToClass(UpdateResTable, res as object)
        if(res.error === true)
        {
        alert(res.error)
        }
        else{
          this.edittable = editresponse.data;
        }
      },
      error=> {},
      ()=> {}
    );

  }

  updateEditTableResponse()
  {
    this.tablebyresidservice.updateTablebyIdApi(classToPlain(this.edittable))
    .subscribe(
      res => {
        const restableresponse = classToPlain(UpdateResTable, res as Object);
        if(res.error === true)
        {
        alert(res.message)
        console.log("api update table res error")
        }
        else{
        this.router.navigate(['/tableinfo'])
        }
      },
      error=> {this.tablebyresidservice.throwError(error)},
      () => {}
    );
  }

}
