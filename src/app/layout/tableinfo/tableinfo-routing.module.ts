import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableinfoComponent } from './tableinfo.component';
import {AddrestTableComponent} from './addrest-table/addrest-table.component';
import {EditRestTableComponent} from './edit-rest-table/edit-rest-table.component';
const routes: Routes = [
  { path: '', component: TableinfoComponent },
  {path: 'addresttable', component: AddrestTableComponent},
  {path: 'editresttable/:id', component: EditRestTableComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TableinfoRoutingModule { }
