import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrestTableComponent } from './addrest-table.component';

describe('AddrestTableComponent', () => {
  let component: AddrestTableComponent;
  let fixture: ComponentFixture<AddrestTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddrestTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrestTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
