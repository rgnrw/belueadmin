import { Component, OnInit } from '@angular/core';
import { Router, RouterModule} from '@angular/router';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {TablebyResIdService} from '../../../services/TablebyResIdService.service';
import {AddTablebyResId, AddTableData} from '../../../model/AddTablebyResId';

@Component({
  selector: 'app-addrest-table',
  templateUrl: './addrest-table.component.html',
  styleUrls: ['./addrest-table.component.scss'],
  providers: [TablebyResIdService],
  exportAs: 'ngForm'
})
export class AddrestTableComponent implements OnInit
{
  addtable: AddTableData;
   jsondata: any;
  resid: number;
  message:boolean;
  constructor(public tablebyresidservice: TablebyResIdService,public router: Router) { }

  ngOnInit() {
    this.addtable= new AddTableData();
    this.resid = +sessionStorage.getItem("id");
    this.addtable.restaurantId= this.resid ;
    console.log(this.addtable.restaurantId);
  }

  postAddTableResponse()
  {
   this.jsondata = classToPlain(this.addtable);
  this.tablebyresidservice.addTablebyIdApi(this.jsondata)
  .subscribe(res => {
    if (res.error === true) { alert(res.message); console.log("Got error in api"); }
   else{
      this.router.navigate(['/tableinfo'])
    }
    },
    error =>{this.tablebyresidservice.throwError(error)},
    () => {}
  );

}
}
