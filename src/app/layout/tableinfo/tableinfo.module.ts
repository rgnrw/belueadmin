import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { TableinfoRoutingModule } from './tableinfo-routing.module';
import { TableinfoComponent } from './tableinfo.component';
import { AddrestTableComponent } from './addrest-table/addrest-table.component';
import { EditRestTableComponent } from './edit-rest-table/edit-rest-table.component';
import { ModalModule } from 'ngx-bootstrap/modal';
@NgModule({
  imports: [
    CommonModule,
    TableinfoRoutingModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  declarations: [TableinfoComponent, AddrestTableComponent, EditRestTableComponent]
})
export class TableinfoModule { }
