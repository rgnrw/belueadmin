import { Component, OnInit, TemplateRef,ElementRef, ViewChild } from '@angular/core';
import { Router, RouterModule} from '@angular/router';
import { routerTransition } from '../../router.animations';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {FormsModule} from '@angular/forms';
import {TablesbyResId, ResTablesData} from '../../model/TablesbyResId';
import {TablebyResIdService} from '../../services/TablebyResIdService.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-tableinfo',
  templateUrl: './tableinfo.component.html',
  styleUrls: ['./tableinfo.component.scss'],
  providers: [TablebyResIdService]
})
export class TableinfoComponent implements OnInit {

  tablelist: ResTablesData[];
  deletemodal = false;
  message: boolean;
  closeResult: string;
 modalRef: BsModalRef; 
 @ViewChild('qrCode')
 qrCode: ElementRef;
  constructor(public router: Router, public tablebyresidservice: TablebyResIdService,
  private modalService: BsModalService) { }

  ngOnInit()
  {
    this.tablebyresidservice.getTablesbyIdApi()
    .subscribe(
      res =>
          {
              const tableResponse = plainToClass(TablesbyResId, res as Object)
              /* console.log(tableResponse); */
              if (res.error === true){
              alert(res.message);
              /* console.log(res.message);
              console.log(res.statusCode);
              console.log(res.error); */
              /* this.router.navigate(['/login']); */
          }
              else
              {
               /*  console.log("api hit success tableinfo"); */
                this.tablelist = tableResponse.data;

              }

      },
       error => {this.tablebyresidservice.throwError(error);},
       () => { }
      );
  }

 openModal(template: TemplateRef<any>) {
   console.log(template);
    this.modalRef = this.modalService.show(template);
  } 


  printQRCode()
  {
    const prtContent = document.getElementById("qrCode");
    const WinPrint = window.open('', '', 'left=50,top=50,width=50,height=50,toolbar=0,scrollbars=0,status=0');
    WinPrint.document.write(prtContent.outerHTML);
    WinPrint.document.close();
    // WinPrint.document.
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
  }

deleteTable(id)
{
this.tablebyresidservice.deleteTablebyIdApi(id)
.subscribe(
res => {
if(res.error === true)
{
  alert(res.message);
}
else
{
this.ngOnInit()
}
 },
error=>{this.tablebyresidservice.throwError(error) },
()=> { }
);
}


}
