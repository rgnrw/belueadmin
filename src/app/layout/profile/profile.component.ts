import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { RestaurantDetail } from './../../model/ResLoginResponse';
import { Component, OnInit } from '@angular/core';
import {AfterViewInit, Directive, ViewChild, ElementRef} from '@angular/core';
import {plainToClass,classToPlain} from 'class-transformer';
import { ProfileService } from './../../services/ProfileService';
import { FileResponse,FileUrl } from './../../model/FileResponse';
import { FileUploadService } from './../../services/FileUpload.service';
import { ResSignupData,ResSignResponse } from './../../model/ResSignUpResponse';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [FileUploadService,ProfileService]
})
export class ProfileComponent implements OnInit {
/* view child start */
  @ViewChild('edit')
  edit: ElementRef;
  @ViewChild('cancel')
  cancel: ElementRef;
  @ViewChild('editform')
  editform: ElementRef;
  @ViewChild('updatebutton')
   updatebutton: ElementRef;
  @ViewChild('fileinput')
  fileinput: ElementRef;

/* view child ends */
  imageError: boolean;
  file: File;
  files: FileList;
  fileUrl: FileUrl;
  profileObj: ResSignupData;
  showImage= false;
  mobNumber: string;
  constructor(public fileservice: FileUploadService, public profileservice: ProfileService) { }
  ngOnInit() {
    this.profileObj = new ResSignupData();
    this.profileObj.name = sessionStorage.getItem('resname');
    this.edit.nativeElement.hidden=false;
    this.getResDetails();
  }


  getResDetails()
  {
  this.profileservice.getResDetailsbyId().subscribe(
    res=> {
      if(res.error === true)
      {
        alert(res.message);
      }
      else{
        const editresponse = plainToClass(ResSignResponse, res as Object);
        this.profileObj = editresponse.data;
        this.mobNumber = this.profileObj.mobileNumbers[0];
      }
    },
    error=>{},
    ()=> {}
  );
  }



 onedit() {
 if(this.edit.nativeElement.id === 'edit')
 {
 this.editform.nativeElement.disabled = false;
  this.updatebutton.nativeElement.hidden = false;
  this.edit.nativeElement.hidden=true;
  this.cancel.nativeElement.hidden=false;
  this.showImage = true;
 }
}

oncancel(){
 if(this.cancel.nativeElement.id === 'cancel')
 {
 this.editform.nativeElement.disabled = true;
  this.updatebutton.nativeElement.hidden = true;
  this.edit.nativeElement.hidden=false;
  this.cancel.nativeElement.hidden=true;
  this.showImage = false;
 }

 }

previewImage(event: any) {
  /* this.isFile = true; */
  this.imageError = false;
  this.profileObj.logo = URL.createObjectURL(event.target.files[0]);
  this.file = event.target.files[0];
  console.log(this.file);
  const filename = this.file.name;
  const formData = new FormData();
  formData.append('file', this.file);
  formData.append('filetype', '1');
  console.log(formData);
  this.fileservice.uploadFile(formData).subscribe(
    res => {
      if (res.error === true) {
        console.log("file upload error");
      }
      else {
        console.log("success");
        const fileresponse = plainToClass(FileResponse, res as Object);
        let fileUrl: FileUrl;
        fileUrl = fileresponse.data;
        console.log(fileUrl);
        this.profileObj.logo = fileUrl.url;
      }
    },
    error => { console.log("file api hit error"); },
    () => { }
  );
  
}

  resImagesUpload(event: any)
  {
    this.file = event.target.files[0];
    const filename = this.file.name;
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('filetype', '1');
    this.fileservice.uploadFile(formData).subscribe(
      res => {
        if (res.error === true) {
          // console.log("file upload error");
        }
        else {
          // console.log("success");
          const fileresponse = plainToClass(FileResponse, res as Object);
          let fileUrl: FileUrl;
          fileUrl = fileresponse.data;
          this.profileObj.images.push(fileUrl.url);
        }
      },
      error => { console.log("file api hit error"); },
      () => { }
    );
  

  }


  deleteResImage(image: string)
  {
    const index= this.profileObj.images.indexOf(image);
    
    this.profileObj.images.splice(index,1);
  }

  openFileBrowser()
  {
    document.getElementById("fileinput").click();
  }

  patchProfileResponse()
  {
    console.log('add image brings me here');
    this.profileObj.username = sessionStorage.getItem('username');
    this.profileObj.mobileNumbers[0]= this.mobNumber;
    this.profileObj.id = +sessionStorage.getItem('id');
    //console.log(this.profileObj);
    this.profileservice.updateRestaurant(this.profileObj).subscribe(
      res=> {
        if(res.error === true)
        {
          alert(res.message);
          //console.log("profile error");
        }
        else{
          this.editform.nativeElement.disabled = true;
          this.cancel.nativeElement.hidden = true;
          this.edit.nativeElement.hidden = false;
          this.showImage = false;
          this.updatebutton.nativeElement.hidden = true;
          this.ngOnInit();  
         // console.log("profile update successfull");
        }
      },
      error=>{},
      ()=> { }
    );
  }


  




}
