import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { plainToClass, classToPlain} from 'class-transformer';
import { ResOrderResponse, ResOrderData} from '../../model/ResOrders';
import { CustomerRequest, CustomerRequestResponse , CustomerSingleRequestResponse} from '../../model/CustomerRequest';
import { HomeService} from '../../services/Home.service';
import { CardService } from '../../services/CardService.service';
import { MessagingService } from '../../messaging.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HomeService]
})
export class HomeComponent implements OnInit {

  current_count= 0;
  initiated_count = 0;
  inprogress_count = 0;
  orderdata: ResOrderData[];
  requestList: CustomerRequest[];
  jsonrequest: any;
  custRequest: CustomerRequest;
  reqCount= 0;
  constructor(private msgService: MessagingService,public router:Router, public homeservice: HomeService, private cardservice: CardService) { }

  ngOnInit() {
    
      
        this.msgService.currentMessage.subscribe(
            res=> {
               if(res)
               {
                this.ngOnInit();
               }
            }
        );
        
    this.homeservice.getAllOrdersApi().subscribe(res => {
            if (res.error === true) {
            } else {
                const orderresponse = plainToClass(
                    ResOrderResponse,
                    res as object
                );
                this.orderdata = orderresponse.data;

                this.current_count = this.orderdata.length;
              
            }
        }, error => {}, () => {
           
        });
        
    this.homeservice.getInitiatedOrdersApi()
      .subscribe(
      res => {
        if (res.error === true) {
          
        }
        else {
          const orderresponse = plainToClass(ResOrderResponse, res as object);
          this.orderdata = orderresponse.data;
          
          this.orderdata.forEach(element => {
            this.initiated_count = this.initiated_count + 1;
            
          });

        }
      },
      error => { },
      () => {  }
      );

    this.homeservice.getInProgressOrdersApi()
      .subscribe(
      res => {
        if (res.error === true) {
         
        }
        else {
          const orderresponse = plainToClass(ResOrderResponse, res as object);
          this.orderdata = orderresponse.data;

          this.orderdata.forEach(element => {
            this.inprogress_count = this.inprogress_count + 1;
          });

        }
      },
      error => { },
      () => { }
      );

      this.getNewRequest();
    

      
  }   /* ng  on it*/


  updateRequest(id,status)
  {
  this.custRequest = new CustomerRequest();
  this.custRequest.id = id;
  this.custRequest.status = status;
  this.jsonrequest = classToPlain(this.custRequest);
  this.homeservice.updateRequest(this.jsonrequest).subscribe(
    res=> {
      if(res.error == true)
      {
        console.log('error');
      }
      else
      {
        console.log('it works');
        // this.getNewRequest();
        this.ngOnInit();
      }
    },
    error=> {},
    () => { }
  );


  }


  getNewRequest()
  {
    this.requestList = null;
      this.homeservice.getInitiatedRequest().subscribe(
        res => { 
          if(res.error == true)
          {
          
          }
          else
          {
          const requestResponse = plainToClass(CustomerRequestResponse, res as Object);
          this.requestList = requestResponse.data;
          this.reqCount = this.requestList.length;
            
          }

        },
        error => { },
        () => {  }
      );

  }



}
