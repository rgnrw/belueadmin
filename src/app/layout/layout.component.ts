import { Component, OnInit, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MessagingService } from '../messaging.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NotificationComponent } from './dashboard/components/notification/notification.component';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit {

    msgObject;
    notifiation: Notification;
    constructor(public toastr: ToastsManager,public vcr:ViewContainerRef,
         public msgService: MessagingService, public router: Router) {
        this.toastr.setRootViewContainerRef(vcr);
     }
      
    ngOnInit() {
        if (this.router.url === '/') {
            this.router.navigate(['/home']);
        }
        this.msgService.getPermission();
        this.msgService.receiveMessage();
        this.msgService.monitorTokenRefresh();
        this.msgService.currentMessage.subscribe(
            res=> {
                console.log('layout subscribe')
            this.msgObject = res;
            console.log(this.msgObject);
            var options = {
                title: this.msgObject.notification.title,
                body: this.msgObject.notification.body,
                icon: 'https://material.io/icons/static/images/icons-180x180.png',
            }
            var n = new Notification(this.msgObject.notification.title, options);
            setTimeout(n.close.bind(n), 10000);
            // console.log('hii notification shown')
            
            }
        );
    }


   

}
