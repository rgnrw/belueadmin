import { Injectable } from '@angular/core';
// import { AngularFirestore, FirebaseRef, Firebase } from 'firebase';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/Subject';
import { Http } from "@angular/http";
import { HttpRequest } from './model/HttpRequest';
import * as global from './global';
import { FCMToken } from './model/ResLoginResponse';
import { CommonServices } from './services/common.service';
import { UPDATE_FCM_TOKEN_URL } from './global';



@Injectable()
export class MessagingService extends CommonServices {
    private messaging = firebase.messaging();
    private messageSource = new Subject();
    notification: FCMToken;
    copyPayload: Object;
    currentMessage = this.messageSource.asObservable(); // message observable to show in Angular component
    constructor(public http: Http) {
      super(http);
    }

    getPermission() {
        this.messaging
            .requestPermission()
            .then(() => {
                // console.log("Notification permission granted.");
                // console.log("Token is :"+this.messaging.getToken());
                return this.messaging.getToken();
            })
            .then(token => {
               console.log(token);
                this.notification = new FCMToken();
                this.notification.notificationToken = token;
                this.updateFCMToken(this.notification);
            })
            .catch(err => {
                console.log("Unable to get permission to notify.", err);
            });
    }

    monitorTokenRefresh()
    {
      console.log('token monitor Function works');
      this.messaging.onTokenRefresh(function() {
          this.messaging
              .getToken()
              .then(function(refreshedToken) {
                  console.log("Token refreshed.");
                 this.notification = new FCMToken();
                 this.notification.notificationToken = refreshedToken;
                 this.updateFCMToken(this.notification);
              })
              .catch(function(err) {
                  console.log("Unable to retrieve refreshed token ", err);
                 
              });
      });


    }



    receiveMessage() {
        console.log("Messaging Function works");
        this.messaging.onMessage(payload => {
            // console.log(payload);
            this.messageSource.next(payload);
        });
    }

    updateFCMToken(notification: any)
    {
      let httpreq = new HttpRequest(global.UPDATE_FCM_TOKEN_URL);
      httpreq.params = notification;
      console.log(httpreq.params);
      httpreq.setPostMethod();
      this.getApi(httpreq).subscribe(res => {
              if (res.error === true) {
              } else {
                  console.log("Token update successfull");
              }
          }, error => {}, () => {});

    }



}
