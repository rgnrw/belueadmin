import * as moment from 'moment';




export class UTCDateTime
{

static getStandardTime(serverdatestr:string, serverformat:string, reqformat:string)
{
    return moment(moment.utc(serverdatestr, serverformat).toDate()).format(reqformat);

}

    static getStandardTimeNew(serverdatestr: string, serverformat: string, reqformat: string) {
        return moment(moment.utc(serverdatestr, serverformat).toDate()).format(reqformat);
        // return moment(moment.utc(serverdatestr, serverformat).toDate()).format(reqformat);

    }

    static getStringDatefromJSON(json: any, reqformat: string) {
    
        return moment(moment(json).toISOString()).format(reqformat);

    }



}

