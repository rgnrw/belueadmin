import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import {AuthTokenStorage} from '../../model/AuthTokenStorage'

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
       /*  if (sessionStorage.getItem('authToken')) {
            return true;
        } */
        if (AuthTokenStorage.getToken()!=null) {
            return true;
        }
        else{
            this.router.navigate(['/login']);
            return false;
        }
    }
}
