import { Component, OnInit } from '@angular/core';
import { Router, RouterModule, Routes, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ResLoginService } from '../../../services/ResLoginService.service';
import { NotificationDetail, NotificationResponse } from '../../../model/PushNotification';
import { plainToClass } from 'class-transformer';
import { NotificationType } from '../../../global';

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"],
    providers: [ResLoginService]
})
export class HeaderComponent implements OnInit {
    pushRightClass: string = "push-right";
    username: string;
    notifications: NotificationDetail[];
    constructor(
        public loginservice: ResLoginService,
        private translate: TranslateService,
        public router: Router
    ) {
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });

        this.username = sessionStorage.getItem("username");
        /* console.log(sessionStorage.getItem("username"));
        console.log(this.username); */
    }

    ngOnInit() {
        this.loginservice.getAllNotifications().subscribe(
            res => {
                if (res.error === true) {

                } else {
                const notificationresponse = plainToClass(NotificationResponse, res as Object);
                this.notifications = notificationresponse.data;
                
                console.log(this.notifications);
            }
            },
            error => {},
            () => {}
        );
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector("body");
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector("body");
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector("body");
        dom.classList.toggle("rtl");
    }

    onLoggedout() {
        sessionStorage.removeItem("token");
        sessionStorage.removeItem("username");
        sessionStorage.removeItem("loginRes");
        sessionStorage.removeItem("id");
        this.router.navigate(["login"]);
    }
    changeLang(language: string) {
        this.translate.use(language);
    }


    callNotifReadApi(notificationItem: NotificationDetail )
    {
    console.log(notificationItem);
    if(notificationItem.read == false)
    {
    console.log('If works');
    const id = notificationItem.id;
    console.log(id);
    this.loginservice.changeReadStatusApi(id).subscribe(
    res=>{
        if(res.error === true)
        {
            console.log('error in api call');
        }
        else
        {
            console.log('read status work');
            this.ngOnInit();
            this.navigateToComponent(+notificationItem.notification.notifType, +notificationItem.notification.referenceId);

        }
     },
    error=> { },
    ()=> {}
    );
    }

    }


    navigateToComponent(type: number,referenceId: number)
    {
        if (type == NotificationType.CUSTOMER_REQUEST) {
            this.router.navigate(["/home"]);
        }

        if (type == NotificationType.NEW_ORDER) {
            this.router.navigate(["/orders/orderdetails",referenceId]);
        }

        if (type == NotificationType.ORDER_STATUS) {
            this.router.navigate(["/orders/orderdetails",referenceId]);
        }

        if (type == NotificationType.CHECK_IN) {
            this.router.navigate(["/tableinfo"]);
        }

        if (type == NotificationType.CHECK_OUT) {
            this.router.navigate(["/tableinfo"]);
        }

    }




}
