import { Component, OnInit } from '@angular/core';
import { Router, RouterModule} from '@angular/router';
import { routerTransition } from '../router.animations';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {FormsModule} from '@angular/forms';
import {ResLoginService} from '../services/ResLoginService.service';
import {ResLoginResponse, ResLoginData, ResLoginCredential} from '../model/ResLoginResponse';
import {AuthTokenStorage} from '../model/AuthTokenStorage';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()],
    providers: [ResLoginService],
    exportAs: 'ngForm'
})
export class LoginComponent implements OnInit {
    reslogin: ResLoginCredential;
    jsonlogincredential: any;
    reslogindata: ResLoginData;
    message = false;
    constructor(public loginservice: ResLoginService, public router: Router) {
    }

    ngOnInit() {
        this.reslogin = new ResLoginCredential();
    }

    postLoginResponse() {
        // console.log(this.reslogin);
        this.jsonlogincredential = classToPlain(this.reslogin);
        this.loginservice.postLoginApi(this.jsonlogincredential)
        .subscribe(res => {
                const resloginresponse = plainToClass(ResLoginResponse , res as object);
                if (res.error === true) {
                 this.message = true;
                }
                else {
                // console.log("api hit success");
                this.reslogindata = resloginresponse.data;
                AuthTokenStorage.saveAuthToken(this.reslogindata);
                 this.router.navigate(['/home'])
                // if(AuthTokenStorage.isAuthTokenValid() == true)
                // {
                //     // localStorage.setItem('isLoggedin', 'true')
                // else { this.router.navigate(['login']) }
                }

            },
            error => { this.loginservice.throwError(error);
                // console.log("api hit error");
                },
            () => {   }
            );



    }




}
