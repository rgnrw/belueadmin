import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class MenubyResId extends BaseResponse {
    @Type(() => ResMenuData)
    data: ResMenuData[];
}
export class ResMenuData {
    name: string;
    restaurantId: number;
    id: number;
    isSelected: boolean;

}
