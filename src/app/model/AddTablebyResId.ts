import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';



export class AddTablebyResId extends BaseResponse {
    @Type(() => AddTableData)
    data: AddTableData;
}

export class AddTableData {
    restaurantId: number;
    tableNumber: number;
    seatingCapacity: number;
}
