import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class ResLoginResponse extends BaseResponse {
    @Type(() => ResLoginData)
    data: ResLoginData;
}
    export class ResLoginData {
        token: string;
        @Type(() => RestaurantDetail)
        restaurantDetail: RestaurantDetail;
    }
    export class ResLoginCredential {
     username: string;
        password: string;
    }
            export class RestaurantDetail {
                logo?: any;
                name: string;
                address: string;
                username: string;
                mobileNumbers: string[];
                images?: any;
                id: number;
            }


        export class FCMToken
        {
        notificationToken: string;
        }