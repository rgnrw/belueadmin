import { ResLoginData } from './ResLoginResponse';

export class AuthTokenStorage {
	static saveAuthToken( res:ResLoginData ){
		sessionStorage.setItem("loginRes", JSON.stringify(res));
		sessionStorage.setItem("token",res.token);
		sessionStorage.setItem("id",res.restaurantDetail.id.toString());
		sessionStorage.setItem("username",res.restaurantDetail.username);
		sessionStorage.setItem("resname",res.restaurantDetail.name);
		 /* console.log(sessionStorage.getItem("username")); */
	}

	static getToken()
	{
		return sessionStorage.getItem("token");
	}

	static getID()
	{
		return sessionStorage.getItem("id");
	}

	static isAuthTokenValid(): boolean {
		if(sessionStorage.getItem('authToken') == null) {
			return false;
		} else {
			return true;
		}
	}
	static getAuthToken():string
	{
		if(sessionStorage.getItem("authToken") != null)
		{ return sessionStorage.getItem("authToken"); }
	}
	constructor( ) { }
}
