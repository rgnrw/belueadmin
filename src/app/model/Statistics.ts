import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class StatResponse extends BaseResponse {
    @Type(() => StatData)
    data: StatData;
}
export class StatData {
    amount: number;
    numOfOrders: number;
}
