import { BaseResponse } from './BaseResponse';
import { Type, plainToClass } from 'class-transformer';
import { ResOrderDetails } from './ResOrderDetail';

export class SessionResponse extends BaseResponse {
    @Type(() => SessionData)
    data: SessionData;
}
export class SessionData {
           checkinId: number;
           userId: number;
           restaurantId: number;
           sessionStatusCode: number;
           tableId: number;
           taxes?: any;
           discounts?: any;
           paidInfo?: any;
           checkinTime: string;
           checkoutTime?: any;
           orders: ResOrderData[];
           @Type(() => Customer)
           customer: Customer;
           @Type(() => Table)
           table: Table;
       }

       export class ResOrderData {
           data: string;
           paymentStatusCode: number;
           paymentStatusText: string;
           id: number;
           checkinId: number;
           orderStatus: number;
           orderStatusText: string;
           created: string;
           remainingAmount: number;
           remainingStatus: boolean;
           createdDate: Date;
           updated: string;
           amount: number;
           orderName: string;
           @Type(() => ResOrderDetails)
           orderItemsArray: ResOrderDetails[];

             @Type(() => Customer1)
           customer: Customer1;
           @Type(() => Table1)
           table: Table1;
    

           parseDataStringToOrderItemsArray() {
               this.orderItemsArray = plainToClass(ResOrderDetails, JSON.parse(this.data));
               console.log(this.orderItemsArray);
           }
       }
       

    export class Customer {
        name: any;
        verified: boolean;
        email: any;
        id: number;
        phoneNumber: string;
        platform: string;
        notificationToken: string;
    }

    export class Table {
        restaurantId: number;
        tableNumber: number;
        seatingCapacity: number;
        id: number;
        qrCodeUrl: string;
        checkInId: number;
        occoupied: boolean;
    }


    export class Customer1 {
        name: any;
        verified: boolean;
        email: any;
        id: number;
        phoneNumber: string;
        platform: string;
        notificationToken: string;
    }

    export class Table1 {
        restaurantId: number;
        tableNumber: number;
        seatingCapacity: number;
        id: number;
        qrCodeUrl: string;
        checkInId: number;
        occoupied: boolean;
    }