import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class TablesbyResId extends BaseResponse {
    @Type(() => ResTablesData)
    data: ResTablesData[];
}
    export class ResTablesData {
        restaurantId: number;
        tableNumber: number;
        seatingCapacity: number;
        id: number;
        qrCodeUrl: string;
        occoupied: boolean;
        checkInId: number;
}
