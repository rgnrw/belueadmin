import { DatePipe } from '@angular/common';
import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';
import { ResOrderDetails} from "./ResOrderDetail";
import {plainToClass} from 'class-transformer';

export class ResOrderResponse extends BaseResponse {
    @Type(() => ResOrderData)
    data: ResOrderData[];
}

export class ResOrderData {
    data:string;
    paymentStatusCode: number;
    paymentStatusText: string;
    id: number;
    checkinId: number;
    orderStatus: number;
    orderStatusText: string;
    created: string;
    remainingAmount: number;
    remainingStatus: boolean;
    createdDate: Date;
    updated: string;
    amount: number;
    orderName: string;
    @Type(() => Customer)
    customer: Customer;
    @Type(() => Table)
    table: Table;
    @Type(() => ResOrderDetails)
    orderItemsArray: ResOrderDetails[];

    parseDataStringToOrderItemsArray(){ 
        this.orderItemsArray = plainToClass(ResOrderDetails, JSON.parse(this.data));
        console.log(this.orderItemsArray);
    }
}

export class UnpaidOrderData
{
    data: string;
    paymentStatusCode: number;
    paymentStatusText: string;
    id: number;
    checkinId: number;
    orderStatus: number;
    orderStatusText: string;
    created: string;
    remainingAmount: number;
    remainingStatus: boolean;
    createdDate: Date;
    updated: string;
    amount: number;
    orderName: string;
    @Type(() => Customer)
    customer: Customer;
    @Type(() => Table)
    table: Table;
    @Type(() => ResOrderDetails)
    orderItemsArray: ResOrderDetails[]; 
}

export class Customer {
    notificationToken?: any;
    verified: boolean;
    id: number;
    phoneNumber: string;
    name: string;
}

export class Table {
    restaurantId: number;
    tableNumber: number;
    seatingCapacity: number;
    id: number;
    qrCodeUrl: string;
    occoupied: boolean;
}


export class CheckInItem
{
    checkinid: number;
    @Type(() => ResOrderData)
    orderdata: ResOrderData[];
}



export class UpdateOrderStatus{
    orderStatusCode: number;
}
