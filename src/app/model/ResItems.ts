import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class ResItems extends BaseResponse {
    @Type(() => ResItemData)
    data: ResItemData[];
}
export class ResItemData {
    name: string;
    unitPrice: number;
    unitQty: number;
    description: string;
    icon: string;
    inStock: boolean;
    qtyInStock: number;
    categoryId: number;
    id: number;
}
