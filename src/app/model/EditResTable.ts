import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';



export class UpdateResTable extends BaseResponse {
    @Type(() => UpdateTableData)
    data: UpdateTableData;
}
export class UpdateTableData {
    restaurantId: number;
    tableNumber: number;
    seatingCapacity: number;
    id: number;
}
