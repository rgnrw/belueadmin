

    export class ResOrderDetails {
        categoryId: number;
        description: string;
        quantity: number;
        id: number;
        inStock: boolean;
        menuId: number;
        name: string;
        qtyInStock: number;
        unitPrice: number;
        unitQty: number;
        csgt: number;
        sgst: number;
        igst: number;
        gst:number;
        amount: number;
        totalTax: number;
        taxAmount: number;
        totalAmount: number;
    }


