import { Http, Request, RequestMethod, Response, Headers, RequestOptions } from '@angular/http';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

export class HttpRequest
{
	url: string;
	method: string;
	params: any;
	taskcode: any;
	headers: Headers;

	constructor(url:string)
	{
		this.url = url;
		this.method = "GET";
		this.headers = new Headers();
		this.addHeader("Content-Type", "application/json");
		 if(AuthTokenStorage.getToken()!=null)
		 {
			 this.addHeader("token",AuthTokenStorage.getToken());
			 console.log(AuthTokenStorage.getToken());
		 }
	}
	setPostMethod()
	{
		this.method = "POST";
	}
	setPutMethod()
	{
		this.method = "PUT";
	}
	setDeleteMethod()
	{
		this.method = "DELETE";
	}

	setPatchMethod()
	{
		this.method = "PATCH";
	}

	addHeader(key:string , value:string)
	{
		this.headers.append(key,value);
	}
	removeHeader(key:string)
	{
		this.headers.delete(key);
	}
}

