import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';



export class AddMenubyResId extends BaseResponse {
    @Type(() => AddMenuData)
    data: AddMenuData;
}

export class AddMenuData {
    name: string;
    restaurantId: number;
}
