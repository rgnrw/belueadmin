import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class FileResponse extends BaseResponse {
    @Type(() => FileUrl)
    data: FileUrl;
}
export class FileUrl {
    url: string;
}
