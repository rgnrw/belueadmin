import { UTCDateTime } from './../Utils/DateTime';
import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class CustomerRequestResponse extends BaseResponse {
    @Type(() => CustomerRequest)
    data: CustomerRequest[];
}

export class CustomerSingleRequestResponse extends BaseResponse {
    @Type(() => CustomerRequest)
    data: CustomerRequest;
}


export class CustomerRequest {
    status: number;
    id: number;
    type: number;
    checkinId: number;
    requestMessage: string;
    @Type(() => Customer)
    customer: Customer;
    @Type(() => Table)
    table: Table;
    requestText: string;
    created: string;


    public get createdDate()
    {
    return UTCDateTime.getStandardTimeNew(this.created, 'DD-MM-YYYY HH:mm:ss','DD-MM-YYYY HH:mm a')
    }

}


export class Customer {
    name?: any;
    notificationToken?: any;
    verified: boolean;
    email?: any;
    id: number;
    phoneNumber: string;
}

export class Table {
    restaurantId: number;
    tableNumber: number;
    seatingCapacity: number;
    id: number;
    qrCodeUrl: string;
    occoupied: boolean;
}
