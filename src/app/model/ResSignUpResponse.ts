import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class ResSignResponse extends BaseResponse {
    @Type(() => ResSignupData)
    data: ResSignupData;
}
    export class ResSignupData {
        logo: string;
        name: string;
        address: string;
        mobileNumbers= new Array<string>();
        username: string;
        password: string;
        images= new Array<String>();
        paymentTypeCode: number;
        id: number;
        cuisine: string;
}
