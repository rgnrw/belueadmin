import { BaseResponse } from "./BaseResponse";
import { Type } from "class-transformer";
import { UTCDateTime } from '../Utils/DateTime';

// export class MenubyResId extends BaseResponse {
//     @Type(() => ResMenuData)
//     data: ResMenuData[];
// }
// export class ResMenuData {
//     name: string;
//     restaurantId: number;
//     id: number;
//     isSelected: boolean;
// }

export class NotificationResponse extends BaseResponse {
           @Type(() => NotificationDetail)
           data: NotificationDetail[];
       }

export class NotificationDetail {
    id: number;
    notification: Notification;
    restaurantId: number;
    created: string;

    public get createTime()
    {
    
    return UTCDateTime.getStandardTime(this.created, "DD-MM-YYYY HH:mm:ss", "DD-MM-YYYY hh:mm a");
    }



    updated: string;
    read: boolean;
}

    export class Notification {
        notifId: number;
        title: string;
        body: string;
        message: string;
        notifType: string;
        referenceId: number;
    }

    