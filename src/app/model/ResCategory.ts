import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class ResCategory extends BaseResponse {
    @Type(() => ResCategoryData)
    data: ResCategoryData[];   
}


export class ResEditCategory extends BaseResponse {
    @Type(() => ResCategoryData)
    data: ResCategoryData;
}
export class ResCategoryData {
    name: string;
    description: string;
    icon: string;
    menuId: number;
    id: number;
    cgst= 0;
    igst= 0;
    sgst=0 ;
    gst= 0;
    totalTax= 0;

           public get cgstValue() {
               return this.cgst;
           }

           public set cgstValue(tax: number) {
               this.cgst = tax;
               this.calcTotalTax();
           }
           public get igstValue() {
               return this.igst;
           }

           public set igstValue(tax: number) {
               this.igst = tax;
               this.calcTotalTax();
           }
           public get sgstValue() {
               return this.sgst;
               
           }

           public set sgstValue(tax: number) {
               this.sgst = tax;
               this.calcTotalTax();
           }

           public set gstValue(tax: number) {
               this.gst = tax;
               this.calcTotalTax();
           }
           
           public get gstValue() {
               return this.gst;
           }

          
           public get totalTaxValue()
           {
            return this.totalTax;
           }


           public calcTotalTax() {
            this.totalTax = this.cgst + this.igst +  this.sgst +  this.gst;
           }
   
}

