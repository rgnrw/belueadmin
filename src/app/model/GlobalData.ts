import { BaseResponse } from './BaseResponse';
import { Type } from 'class-transformer';

export class GlobalResponse extends BaseResponse {
    @Type(() => GlobalData)
    data: GlobalData;
}
export class GlobalData
{
    paymentMode= new Array<PaymentMode>(); 

}

export class PaymentMode {
  text: string;
  code: number;
}

