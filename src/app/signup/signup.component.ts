import { Component, OnInit } from '@angular/core';
import { Router, RouterModule} from '@angular/router';
import { routerTransition } from '../router.animations';
import { plainToClass } from 'class-transformer';
import { classToPlain } from 'class-transformer';
import {FormsModule} from '@angular/forms';
import {ResSignUpService} from '../services/ResSignUpService.service';
import {ResSignResponse, ResSignupData} from '../model/ResSignUpResponse';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    providers: [ResSignUpService],
    animations: [routerTransition()],
    exportAs: 'ngForm'
})
export class SignupComponent implements OnInit {

    responseObject: ResSignResponse;
    ressignup: ResSignupData;
    message: boolean;
    mobilenumber: string;
    mobNumber = Array<string>();

    constructor(public ressignupservice: ResSignUpService, private router: Router) { }

    ngOnInit() {
        this.ressignup = new ResSignupData();
    }
    postResSignResponse() {
    this.mobNumber.push(this.mobilenumber);
    console.log(this.mobNumber);
    this.ressignup.mobileNumbers = this.mobNumber;
    // this.ressignup.mobileNumbers.push(this.mobilenumber);
    // console.log(this.ressignup);
    this.ressignupservice.postSignUpApi(this.ressignup)
    .subscribe(res => {
          if (res.error === true) { alert(res.message); console.log("Got error in api"); }
            // tslint:disable-next-line:one-line
            else {
                /* localStorage.setItem('isLoggedin', 'true');
                console.log("api hit success"); */

        this.router.navigate(['/login']) }
        },
        error => { this.ressignupservice.throwError(error); },
        () => {   }
        );
}
onFocus(event: any) {
this.message = false;
}
    }
