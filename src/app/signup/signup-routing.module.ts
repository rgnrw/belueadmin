import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup.component';
/* import {DashboardComponent} from '../layout/dashboard/dashboard.component'; */
const routes: Routes = [
    { path: '', component: SignupComponent },
  /*   { path: 'dashboard', component: DashboardComponent} */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignupRoutingModule {

    constructor() {
        console.log('Signup routing module is loading in memory');
    }
 }
