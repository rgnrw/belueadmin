import { PaymentMode } from './model/GlobalData';
import { UTCDateTime } from './Utils/DateTime';
'use strict';

 export const API_BASE_URL: string = 'api.beluepartner.com'; 
/* export const API_BASE_URL: string = '192.168.1.17:8081/belue'; */

export const value: string = 'true';

export const GET_SIGNUP_URL: string = 'https://'+API_BASE_URL+'/restaurant/save';

export const GET_LOGIN_URL: string = 'https://'+API_BASE_URL+'/restaurant/login';

export const GET_RES_TABLE_URL: string = 'https://'+API_BASE_URL+'/restaurantTable/restaurant/';

export const GET_TABLE_BY_ID_URL: string = 'https://' + API_BASE_URL + '/restaurantTable/';

export const SAVE_RES_TABLE_URL: string = 'https://'+API_BASE_URL+'/restaurantTable/save';

export const DEL_RES_TABLE_URL: string = 'https://' + API_BASE_URL + '/restaurantTable/';

export const UPDATE_RES_TABLE_URL: string = 'https://' + API_BASE_URL + '/restaurantTable/update';

export const GET_RES_MENU_URL: string = 'https://' + API_BASE_URL + '/menu/restaurant/';

export const GET_MENU_BY_MENU_ID_URL: string = 'https://' + API_BASE_URL + '/menu/';

export const SAVE_RES_MENU_URL: string = 'https://' + API_BASE_URL + '/menu/save';

export const UPDATE_RES_MENU_URL: string = 'https://' + API_BASE_URL + '/menu/update';

export const DEL_RES_MENU_URL: string = 'https://' + API_BASE_URL + '/menu/';

export const GET_RES_CTG_URL: string = 'https://' + API_BASE_URL + '/menucategory/menu/';

export const GET_CTG_BY_CTGID_URL: string = 'https://' + API_BASE_URL + '/menucategory/';

export const DEL_RES_CTG_URL: string = 'https://' + API_BASE_URL + '/menucategory/';

export const SAVE_RES_CTG_URL: string = 'https://' + API_BASE_URL + '/menucategory/save';

export const UPDATE_RES_CTG_URL: string = 'https://' + API_BASE_URL + '/menucategory/update';

export const GET_RES_ITEM_URL: string = 'https://' + API_BASE_URL + '/menuitem/menucategory/';

export const GET_ITEM_BY_ITEM_URL: string = 'https://' + API_BASE_URL + '/menuitem/';

export const DEL_RES_ITEM_URL: string = 'https://' + API_BASE_URL + '/menuitem/';

export const SAVE_RES_ITEM_URL: string = 'https://' + API_BASE_URL + '/menuitem/save';

export const UPDATE_RES_ITEM_URL: string = 'https://' + API_BASE_URL + '/menuitem/update';
 
/* Initiated Orders of the restaurant */
export const GET_RES_ORDER_URL: string = 'https://' + API_BASE_URL + '/order/new?user=1&status=1&history=0&id=';

export const GET_ALL_CHEKEDIN_ORDER_URL: string =
    "https://" + API_BASE_URL + "/order/new?user=1&status=0&history=0&id=";

/* In Progress of the restaurant */
export const GET_InProgress_ORDERS_URL: string = 'https://' + API_BASE_URL + '/order/new?user=1&status=2&history=0&id=';

export const GET_COMPLETED_ORDERS_URL: string =
    "https://" + API_BASE_URL + "/order/new?user=1&status=3&history=0&id=";


export const GET_CANCELLED_ORDERS_URL: string =
    "https://" + API_BASE_URL + "/order/new?user=1&status=4&history=0&id=";

export const GET_RES_ORDER_BY_CHECKINID: string = 'https://' + API_BASE_URL + '/order/chekinId/';

/* Completed Orders of the restaurant */
export const GET_ORDER_HISTORY: string = 'https://' + API_BASE_URL + '/order/new?user=1&status=0&history=1&id=';


export const GET_INITIATED_REQUEST: string = 'https://' + API_BASE_URL + '/customerRequest/restaurant/';

export const UPDATE_CUSTOMER_REQUEST: string = 'https://' + API_BASE_URL + '/customerRequest/update';

export const GET_GLOBAL_DATA: string = 'https://' + API_BASE_URL + '/globaldata/';

export const CHECKOUT_URL: string = 'https://' + API_BASE_URL + '/customer/session/checkout';

export const UPDATE_RES_URL: string = 'https://' + API_BASE_URL + '/restaurant/update';

export const GET_RES_BY_RES_ID: string = 'https://' + API_BASE_URL + '/restaurant/';

export const UPDATE_ORDER_STATUS_URL: string = 'https://' + API_BASE_URL + '/order/orderStatus/';

/* upload image */

export const UPLOAD_IMAGE_URL: string = 'https://' + API_BASE_URL + '/uploadFile/';

export const CHANGE_PASSWORD_URL: string = 'https://' + API_BASE_URL + '/restaurant/changePassword';

export const GET_CUSTOMER_SESSION: string = 'https://' + API_BASE_URL + '/customer/session/';

export const GET_STATISTICS_URL: string = 'https://' + API_BASE_URL + '/restaurant/';

export const Test_api_global: string = "https://" + "default-environment.dnvv7k2pbr.ap-south-1.elasticbeanstalk.com/tripleplay" + "/globaldata/";


export const UPDATE_FCM_TOKEN_URL: string = "https://" + API_BASE_URL + "/restaurant/notificationToken";

export const GET_ALL_NOTIFICATIONS_URL: string = "https://" + API_BASE_URL + "/restaurant/notifications";


export const READ_NOTIFICATION_URL: string = "https://" + API_BASE_URL + "/restaurant/notifications/";


export enum OrderStatusCode
{
Initated = 1,
InProgress,
Completed,
Cancelled
}

export enum PaymentStatusCode
{
PAID=1,
UNPAID,
PARTIALLY_PAID
}

export enum SessionStatusCode
{
CHECKED_IN=1,
CHECKED_OUT
}



export enum NotificationType
{
CUSTOMER_REQUEST=1,
NEW_ORDER,
ORDER_STATUS,
CHECK_IN,
CHECK_OUT,
PAYMENT	
}