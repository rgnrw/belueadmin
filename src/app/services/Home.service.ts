import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()
export class HomeService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }
    /* current/all orders */
    getAllOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(global.GET_ALL_CHEKEDIN_ORDER_URL.concat(id));
        return this.getApi(httpreq);
    }

    getInitiatedOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(global.GET_RES_ORDER_URL.concat(id));
        return this.getApi(httpreq);
    }

    getInProgressOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_InProgress_ORDERS_URL.concat(id)
        );
        return this.getApi(httpreq);
    }

    getInitiatedRequest() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_INITIATED_REQUEST.concat(id).concat("?status=1")
        );
        return this.getApi(httpreq);
    }

    updateRequest(jsondata) {
        const httpreq = new HttpRequest(global.UPDATE_CUSTOMER_REQUEST);
        httpreq.setPatchMethod();
        httpreq.params = jsondata;
        return this.getApi(httpreq);
    }

    getStatistics(startdate, enddate) {
         const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_STATISTICS_URL.concat(id).concat('/stats?').concat("startDate=")
                .concat(startdate)
                .concat("&endDate=")
                .concat(enddate)
        );

        return this.getApi(httpreq);
    }
}
