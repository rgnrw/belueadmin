import {  Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import {AuthTokenStorage} from '../model/AuthTokenStorage';

@Injectable()

export class TablebyResIdService extends CommonServices {
	constructor(public http:Http) {
		super(http);
	}
	getTablesbyIdApi()
	{
		// const id = AuthTokenStorage.getID();
		const httpreq = new HttpRequest(global.GET_RES_TABLE_URL.concat(AuthTokenStorage.getID()));
		// httpreq.removeHeader("Content-Type");
		return this.getApi(httpreq);
	}

	addTablebyIdApi(jsonAddTableData)
	{
		const httpreq = new HttpRequest(global.SAVE_RES_TABLE_URL);
		httpreq.setPostMethod();
		httpreq.params=jsonAddTableData;
		return this.getApi(httpreq);
	}

	deleteTablebyIdApi(id)
	{
		const httpreq= new HttpRequest(global.DEL_RES_TABLE_URL.concat(id));
		httpreq.setDeleteMethod();
		return this.getApi(httpreq);
	}
	updateTablebyIdApi(jsonupdatedata:any)
	{
	const httpreq= new HttpRequest(global.UPDATE_RES_TABLE_URL);
		httpreq.setPatchMethod();
		httpreq.params=jsonupdatedata;
		return this.getApi(httpreq);
	}

	getTablebyIdApi(id)
	{
	const httpreq = new HttpRequest(global.GET_TABLE_BY_ID_URL.concat(id));
	return this.getApi(httpreq);
	}


}
