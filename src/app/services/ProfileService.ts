import { GET_InProgress_ORDERS_URL, CHECKOUT_URL, UPDATE_RES_URL, GET_RES_BY_RES_ID } from './../global';
import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()

export class ProfileService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }

   

    updateRestaurant(jsondata)
    {
    const httpreq = new HttpRequest(global.UPDATE_RES_URL);
    httpreq.params= jsondata;
    httpreq.setPatchMethod();
    return this.getApi(httpreq);
    }


    getResDetailsbyId()
    {
    const httpreq = new HttpRequest(global.GET_RES_BY_RES_ID.concat(sessionStorage.getItem('id')));
    return this.getApi(httpreq);
    }
    

}
