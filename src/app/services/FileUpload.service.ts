import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()

export class FileUploadService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }


    uploadFile(formData: FormData) {
        // console.log(formData);
        const httpreq = new HttpRequest(global.UPLOAD_IMAGE_URL);
        httpreq.params = formData;
        httpreq.removeHeader("Content-Type");
        httpreq.setPostMethod();
        return this.getApi(httpreq);
    }




}



