import { GET_InProgress_ORDERS_URL, CHECKOUT_URL, UPDATE_RES_URL, GET_RES_BY_RES_ID } from './../global';
import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()
export class CardService extends CommonServices {
    requests: number;
    current_orders: number;
    new_orders: number;
    order_inprogress: number;
    constructor(public http: Http) {
        super(http);
    }

    setRequests(requests: number) {
        this.requests = requests;
    }

    setCurrentOrders(all_orders: number) {
        this.current_orders = all_orders;
    }

    setNewOrders(new_orders: number) {
        this.new_orders = new_orders;
    }

    setInProgressOrders(orders_inprogress: number) {
        this.order_inprogress = orders_inprogress;
    }
}
