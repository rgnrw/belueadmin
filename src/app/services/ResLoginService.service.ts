import {  Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import {AuthTokenStorage} from '../model/AuthTokenStorage';
import { UPDATE_FCM_TOKEN_URL, GET_ALL_NOTIFICATIONS_URL } from '../global';

@Injectable()
export class ResLoginService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }
    postLoginApi(jsonResSignUpData: any) {
        let httpreq = new HttpRequest(global.GET_LOGIN_URL);
        // console.log(global.GET_LOGIN_URL);
        httpreq.params = jsonResSignUpData;
        httpreq.setPostMethod();
        return this.getApi(httpreq);
    }

   fcmTokenService(jsontoken: any)
   {
         let httpreq = new HttpRequest(global.UPDATE_FCM_TOKEN_URL);
         httpreq.params = jsontoken;
        //  console.log(httpreq.params);
         httpreq.setPostMethod();
         return this.getApi(httpreq);
   }


    getAllNotifications()
    {
     let httpreq = new HttpRequest(global.GET_ALL_NOTIFICATIONS_URL);
     return this.getApi(httpreq);

    }



    changeReadStatusApi(id)
    {
     let httpreq = new HttpRequest(global.READ_NOTIFICATION_URL.concat(id));
     httpreq.setPatchMethod();
     return this.getApi(httpreq);
    }



}
