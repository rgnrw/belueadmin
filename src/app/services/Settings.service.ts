import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';

@Injectable()

export class SettingsService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }

    changePasswordApi(jsonData: any) {
        const httpreq = new HttpRequest(global.CHANGE_PASSWORD_URL);
        httpreq.params = jsonData;
         httpreq.setPostMethod();
        return this.getApi(httpreq);
    }
    
}
