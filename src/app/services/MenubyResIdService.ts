import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()

export class MenubyResIdService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }

      /* this will fetch all menu */
    getMenubyIdApi()
    {
        const httpreq= new HttpRequest(global.GET_RES_MENU_URL.concat(AuthTokenStorage.getID()));
        /* console.log(httpreq.url); */
        httpreq.removeHeader("token");
        return this.getApi(httpreq);
    }

    addMenubyIdApi(jsonmenudata:any)
    {
    const httpreq= new HttpRequest(global.SAVE_RES_MENU_URL);
    httpreq.setPostMethod();
    httpreq.params=jsonmenudata;
    // console.log(httpreq.url);
    return this.getApi(httpreq);
    }

    updateMenubyIdApi(jsonmenupdate: any)
    {
    const httpreq = new HttpRequest(global.UPDATE_RES_MENU_URL);
    httpreq.setPatchMethod();
    httpreq.params= jsonmenupdate;
    return this.getApi(httpreq);
    }


    deleteMenubyIdApi(id)
    {
        const httpreq = new HttpRequest(global.DEL_RES_MENU_URL.concat(id));
        httpreq.setDeleteMethod();
        return this.getApi(httpreq);
    }


    getMenubyMenuIdApi(menuid)
    {
    const httpreq = new HttpRequest(global.GET_MENU_BY_MENU_ID_URL.concat(menuid));
    return this.getApi(httpreq);


    }





}
