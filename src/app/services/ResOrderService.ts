import { GET_InProgress_ORDERS_URL, CHECKOUT_URL, UPDATE_ORDER_STATUS_URL } from './../global';
import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';
import { GET_ALL_CHEKEDIN_ORDER_URL } from '../global';


@Injectable()
export class ResOrderService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }

    /* this will fetch initiated orders of the restaurant*/
    getResOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(global.GET_RES_ORDER_URL.concat(id));
        return this.getApi(httpreq);
    }

    getAllCheckedInOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_ALL_CHEKEDIN_ORDER_URL.concat(id)
        );
        return this.getApi(httpreq);
    }

    getInProgressResOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_InProgress_ORDERS_URL.concat(id)
        );
        return this.getApi(httpreq);
    }

    getCompletedResOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_COMPLETED_ORDERS_URL.concat(id)
        );
        return this.getApi(httpreq);
    }

     getCancelledResOrdersApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_CANCELLED_ORDERS_URL.concat(id)
        );
        return this.getApi(httpreq);
    }


    getResOrderByCheckinIdApi(id) {
        const httpreq = new HttpRequest(
            global.GET_RES_ORDER_BY_CHECKINID.concat(id)
        );
        return this.getApi(httpreq);
    }

    getOrderHistoryApi() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(global.GET_ORDER_HISTORY.concat(id));
        return this.getApi(httpreq);
    }

    getGlobalDataApi() {
        const httpreq = new HttpRequest(global.GET_GLOBAL_DATA);
        return this.getApi(httpreq);
    }

    checkOut(jsondata) {
        const httpreq = new HttpRequest(global.CHECKOUT_URL);
        httpreq.setPostMethod();
        httpreq.params = jsondata;
        return this.getApi(httpreq);
    }

    changeOrderStatus(id, jsondata) {
        const httpreq = new HttpRequest(
            global.UPDATE_ORDER_STATUS_URL.concat(id)
        );
        httpreq.setPatchMethod();
        httpreq.params = jsondata;
        return this.getApi(httpreq);
    }

    getCustomerSession(id) {
        const httpreq = new HttpRequest(global.GET_CUSTOMER_SESSION.concat(id));
        return this.getApi(httpreq);
    }

    getInitiatedRequest() {
        const id = sessionStorage.getItem("id");
        const httpreq = new HttpRequest(
            global.GET_INITIATED_REQUEST.concat(id).concat("?status=1")
        );
        return this.getApi(httpreq);
    }
}
