import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()

export class ResItemService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }

    getItemsbyIdApi(catid)
    {
        const httpreq = new HttpRequest(global.GET_RES_ITEM_URL.concat(catid));
        return this.getApi(httpreq);
    }


    deleteItembyIdApi(itemid)
    {
    const httpreq = new HttpRequest(global.DEL_RES_ITEM_URL.concat(itemid));
    httpreq.setDeleteMethod();
    return this.getApi(httpreq);

    }


    addItembyIdApi(jsondata: any)
    {
        const httpreq = new HttpRequest(global.SAVE_RES_ITEM_URL);
        httpreq.params= jsondata;
        console.log(httpreq.url);
        httpreq.setPostMethod();
        return this.getApi(httpreq);
    }


    updateItembyIdApi(jsondata: any) {
        const httpreq = new HttpRequest(global.UPDATE_RES_ITEM_URL);
        httpreq.params = jsondata;
        console.log(httpreq.url);
        httpreq.setPatchMethod();
        return this.getApi(httpreq);
    }

    getItembyItemIdApi(itemid)
    {
    const httpreq = new HttpRequest(global.GET_ITEM_BY_ITEM_URL.concat(itemid));
    return this.getApi(httpreq);
    }

}
