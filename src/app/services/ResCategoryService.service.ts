import { Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';
import { AuthTokenStorage } from '../model/AuthTokenStorage';

@Injectable()

export class ResCategoryService extends CommonServices {
    constructor(public http: Http) {
        super(http);
    }


    getCategorybyIdApi(menuid)
    {
        const httpreq = new HttpRequest(global.GET_RES_CTG_URL.concat(menuid));
       /*  httpreq.removeHeader("token"); */
        return this.getApi(httpreq);
    }


    deleteCategorybyidApi(id)
    {
    const httpreq = new HttpRequest(global.DEL_RES_CTG_URL.concat(id));
    httpreq.setDeleteMethod();
    return this.getApi(httpreq);
    }


    addCategorybyIdApi(jsondata:any)
    {
    const httpreq = new HttpRequest(global.SAVE_RES_CTG_URL);
    httpreq.setPostMethod();
    httpreq.params= jsondata;
    return this.getApi(httpreq);
    }

    updateCategorybyIdApi(jsondata: any)
    {
    const httpreq = new HttpRequest(global.UPDATE_RES_CTG_URL);
    httpreq.setPatchMethod();
    httpreq.params= jsondata;
    return this.getApi(httpreq);
    }


    getCategoryDetailbyIdApi(catid)
    {
        const httpreq = new HttpRequest(global.GET_CTG_BY_CTGID_URL.concat(catid));
        return this.getApi(httpreq);

    }



}



