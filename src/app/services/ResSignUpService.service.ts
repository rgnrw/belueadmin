import {  Injectable } from '@angular/core';
import { HttpRequest } from '../model/HttpRequest';
import { CommonServices } from '../services/common.service';
import { Http } from '@angular/http';
import * as global from '../global';

@Injectable() 

export class ResSignUpService extends CommonServices
{
	constructor(public http:Http)
	{
		super(http);
	}	
	postSignUpApi(jsonResSignUpData: any)
	{
		let httpreq = new HttpRequest(global.GET_SIGNUP_URL);
		// console.log(global.GET_SIGNUP_URL);
		httpreq.params = jsonResSignUpData;
		// console.log(httpreq.params);
		httpreq.setPostMethod();
		return this.getApi(httpreq);
	}
}