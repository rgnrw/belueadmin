import { Injectable } from '@angular/core';
import { Http, Request, RequestMethod, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpRequest } from '../model/HttpRequest';
// import * as moment from "moment";

@Injectable()
export class CommonServices{
	method: string;
	constructor(public http: Http) {}
	getApi(httpreq : HttpRequest)
	// tslint:disable-next-line:one-line
	// tslint:disable-next-line:indent
	{

		this.method = httpreq.method;
		let options = new RequestOptions({ headers: httpreq.headers});
		console.log(options);
		if(this.method === "GET")
		{ 
		return this.http.get(httpreq.url, options)
						.map(res => res.json());
		}
		else if(this.method == "POST")
		{
		return this.http.post(httpreq.url, httpreq.params, options)
		                .map(res => res.json());
		}
		else if(this.method == "PUT")
		{
		return this.http.put(httpreq.url, httpreq.params, options)
	                    .map(res => res.json());
		}
		else if(this.method == "DELETE")
		{
		return this.http.delete(httpreq.url, options)
        	            .map(res => res.json());
		}

		else if (this.method == "PATCH") {
			return this.http.patch(httpreq.url,httpreq.params ,options)
				.map(res => res.json());
		}



	}
	throwError(error: any)
	{
      	if (error != "")
      	{
	      	alert("Server Error\n"+ error + "\nYou may see dummy data or nothing");
      	}
  	}
}
